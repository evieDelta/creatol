module codeberg.org/eviedelta/creatol

go 1.16

require (
	emperror.dev/errors v0.8.0
	github.com/disintegration/imaging v1.6.2
	github.com/go-chi/chi/v5 v5.0.7
	github.com/gorilla/csrf v1.7.1 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/jackc/pgconn v1.10.0 // indirect
	github.com/jackc/pgtype v1.8.1 // indirect
	github.com/jackc/pgx/v4 v4.13.0 // indirect
	github.com/kolesa-team/go-webp v1.0.1
	github.com/muesli/smartcrop v0.3.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pelletier/go-toml/v2 v2.0.0-beta.4
	github.com/rubenv/sql-migrate v0.0.0-20210614095031-55d5740dbbcc // indirect
	go.etcd.io/bbolt v1.3.6
	golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac
	lukechampine.com/blake3 v1.1.5
)
