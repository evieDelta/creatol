package creatol

type Role struct {
	ID    RoleID
	Name  string
	Color uint32
	Icon  IconRef
}
