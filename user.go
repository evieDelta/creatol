package creatol

import (
	"time"

	"codeberg.org/eviedelta/creatol/constants"
)

type UserSession struct {
	ID           SessionID
	UserID       UserID
	Key          []byte
	InvalidAfter time.Time
	LastUsed     time.Time
	IPAddress    string
	UserAgent    string
}

// UserAuth is for the authentication bits of a user
type UserAuth struct {
	ID       UserID // internal ID used for all internal functions
	SourceID string // login source ID (probably a user ID obtained via oauth2, could also be an email or username if that method is used)

	AuthType constants.AuthMethod
	Passkey  []byte
	Meta     []byte
}

// UserProfile is the bits that will be actually public on a user
type UserProfile struct {
	ID     UserID
	Name   string
	Avatar string // a users avatar, contains a hash that represents an image on the media thingy

	Joined time.Time

	Roles []RoleID
}

// UserProfileMeta contains a little extra info on a user used internally
type UserProfileMeta struct {
	UserProfile

	Managed      bool
	AvatarSource string // only used for managed avatars, is the identifier used by the source
}

// FormatName returns a version of their username appended by their ID
func (u UserProfile) FormatName() string {
	return u.Name + " #" + u.ID.String()
}
