package creatol

import (
	"context"
	"fmt"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	"codeberg.org/eviedelta/creatol/constants"
	"codeberg.org/eviedelta/creatol/internal/stopwatch"
)

// Database .
type Database interface {
	Open(ctx context.Context, connstr string) error
	Close(ctx context.Context) error

	UserFullProfileGetByID(context.Context, UserID) (UserProfileMeta, error)
	UserProfileGetByID(context.Context, UserID) (UserProfile, error)
	UserProfileUpdate(context.Context, UserProfileMeta) error

	// the only non-auth specific auth db function
	// this does not return the actual session key
	// and what it returns should only be shown to the user it belongs to
	// or to high level admins in circumstances where it is absolutely required
	GetSessionsByUser(context.Context, UserID) ([]UserSession, error)
}

// AuthDatabase is for functions to be used exclusively by the auth package
// please try to keep all use of these centralised to the auth package and
// avoid touching these from outside of the auth package
// if a function not given by auth is needed, add it to auth instead
// this is because auth needs to know if something changes because of cache
type AuthDatabase interface {
	// Add User
	RegisterUser(context.Context, UserAuth, UserProfileMeta) error

	GetUserAuthBySource(ctx context.Context, sourceID string, kind constants.AuthMethod) (UserAuth, error)
	//  GetUserAuthByID(ctx context.Context, id UserID) (UserAuth, error)
	UpdateUserAuth(context.Context, UserAuth, UserAuth) error
	DeleteAccount(ctx context.Context, id UserID, delname string) error

	NewSession(context.Context, UserSession) error
	GetSession(context.Context, []byte) (UserSession, UserAuth, error)
	UpdateSession(ctx context.Context, id SessionID, expire, lastuse time.Time) error
	PruneSessions(context.Context) error
	InvalidateSession(context.Context, SessionID) error
	InvalidateSessionKey(context.Context, []byte) (SessionID, error)

	UserFullProfileGetByID(context.Context, UserID) (UserProfileMeta, error)
	UserProfileGetByID(context.Context, UserID) (UserProfile, error)
	UserProfileUpdate(context.Context, UserProfileMeta) error
}

type QueryStatKeyType struct{}

var QueryStatKey QueryStatKeyType

func GetQueryStat(ctx context.Context) *QueryStat {
	v := ctx.Value(QueryStatKey)
	fmt.Printf("get query stat %p", v)
	if v == nil {
		fmt.Println(" none")
		return &QueryStat{Placeholder: true}
	}
	fmt.Println(" is one")

	return v.(*QueryStat)
}

type QueryStat struct {
	Mu      sync.Mutex
	Count   uint
	Queries []QueryTime
	MetaQs  []QueryTime

	Placeholder bool
}

type QueryTime struct {
	Name string
	Func string
	File string
	Time time.Duration
	Tx   bool
}

func (q *QueryStat) Query(name string) (finish func()) {
	if q.Queries == nil {
		q.Mu.Lock()
		q.Queries = make([]QueryTime, 0)
		q.Mu.Unlock()
	}

	fmt.Println("call to query:")
	for i := 1; ; i++ {
		pc, _, li, ok := runtime.Caller(i)
		if !ok {
			break
		}

		fn := runtime.FuncForPC(pc)
		funna := fn.Name()

		if !strings.HasPrefix(funna, "codeberg.org/evie") {
			break
		}

		fmt.Printf("\t%v :%v\n", funna, li)
	}
	fmt.Println("")

	pc, fl, li, _ := runtime.Caller(1)
	fn := runtime.FuncForPC(pc)
	funna := fn.Name()
	funna = funna[strings.LastIndex(funna, "/"):]

	st := stopwatch.NewAndStart()
	return func() {
		q.Mu.Lock()
		defer q.Mu.Unlock()

		d := st.Stop()
		q.Count++
		q.Queries = append(q.Queries, QueryTime{name, funna, filepath.Base(fl) + ":" + strconv.Itoa(li), d, false})
	}
}

func (q *QueryStat) Meta(name string) (finish func()) {
	if q.Queries == nil {
		q.Mu.Lock()
		q.Queries = make([]QueryTime, 0)
		q.Mu.Unlock()
	}

	fmt.Println("call to meta:")
	for i := 1; ; i++ {
		pc, _, li, ok := runtime.Caller(i)
		if !ok {
			break
		}

		fn := runtime.FuncForPC(pc)
		funna := fn.Name()

		if !strings.HasPrefix(funna, "codeberg.org/evie") {
			break
		}

		fmt.Printf("\t%v :%v\n", funna, li)
	}

	pc, fl, li, _ := runtime.Caller(1)
	fn := runtime.FuncForPC(pc)
	funna := fn.Name()
	funna = funna[strings.LastIndex(funna, "/"):]

	st := stopwatch.NewAndStart()
	return func() {
		q.Mu.Lock()
		defer q.Mu.Unlock()

		d := st.Stop()
		q.MetaQs = append(q.MetaQs, QueryTime{name, funna, filepath.Base(fl) + ":" + strconv.Itoa(li), d, true})
	}
}

func (q *QueryStat) String() string {
	fmt.Println("q stat string call")

	if q.Count == 0 {
		return ""
	}

	q.Mu.Lock()
	defer q.Mu.Unlock()

	var totaldur time.Duration

	for _, x := range q.Queries {
		totaldur += x.Time
	}

	avgdur := totaldur / time.Duration(len(q.Queries))

	s := &strings.Builder{}
	fmt.Fprintf(s, "Queries: %v, Total Time: %v", q.Count, totaldur.Round(time.Millisecond))
	if q.Count > 1 {
		fmt.Fprintf(s, ", Average Time: %v", avgdur.Round(time.Millisecond))
	}
	s.WriteRune('\n')

	cf := ""
	var total time.Duration
	var count int
	for _, x := range q.Queries {
		if x.Func != cf {
			if count >= 1 {
				fmt.Fprintf(s, " ^ C: %v, TT: %v, AT: %v\n", count, total.Round(time.Millisecond), (total / time.Duration(count)).Round(time.Millisecond))
			} else {
				fmt.Fprintln(s)
			}
			fmt.Fprintf(s, "%v (%v)\n", x.Func, x.File)
			cf = x.Func
			total = 0
			count = 0
		}
		count++
		total += x.Time
		let := "Q"
		if x.Tx {
			let = "M"
		}
		fmt.Fprintf(s, " | %v: %v %v\n", x.Name, let, x.Time.Round(time.Millisecond))
	}
	if count >= 1 {
		fmt.Fprintf(s, " ^ C: %v, TT: %v, AT: %v\n", count, total.Round(time.Millisecond), (total / time.Duration(count)).Round(time.Millisecond))
	} else {
		fmt.Fprintln(s)
	}

	return s.String()
}
