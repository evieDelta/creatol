package creatol

import "io"

//go:generate go run codeberg.org/eviedelta/creatol/sid/gensid -genfuncs -timefunc -out ./idtypes.go TagID MakerID UserID AuditEntryID RoleID IconID SessionID

// MediaFiles ,
type MediaFiles interface {
	// The standard is to use toml for decoding, but as a raw string is passed it can be anything it needs to be
	Initialise(config string) error

	// Get a default version of a config
	GetDefaultConfig() string

	// Encode and store an image into the media store
	// format is the file extension, without the leading . (so "png" instead of ".png")
	ImgStore(data io.Reader, format string) (name string, err error)

	// Delete an image from the store
	ImgDelete(name string) error

	// these endpoints take an image identifier and return formatted how they should be found on the media server
	// this should not return a result including the url root, the url root is added on by the site

	// the normal view for an image
	EndpointImgNormal(name string) string
	// the full size view of an image, in case the store by default sizes down large images
	EndpointImgOriginal(name string) string
	// a thumbnail size view
	EndpointImgThumbnail(name string) string
}
