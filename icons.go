package creatol

import (
	"unicode/utf8"
)

// IconRef contains a prefix that is exactly 1 char long and a icon body
// the prefix can one of the follows
// - ~ | reference by name a static icon built into the service
//     | such as _feather#star for the star icon from the feather set
// - + | a raw utf8 string
// - : | reference an emote by name
// - x | select a custom icon from the icon database via its unique ID
//     | such as x1895076389a148c

// IconRef types
const (
	IconRefTypeInvalid   = 0
	IconRefTypeBuiltin   = '~'
	IconRefTypeUTF8      = '+'
	IconRefTypeEmojiName = ':'
	IconRefTypeCustom    = 'x'
)

type IconRef string

func (i IconRef) Type() rune {
	if len(i) <= 1 {
		return IconRefTypeInvalid // nil
	}
	switch []rune(i)[0] {
	case IconRefTypeBuiltin:
		return IconRefTypeBuiltin
	case IconRefTypeCustom:
		return IconRefTypeCustom
	case IconRefTypeUTF8:
		return IconRefTypeUTF8
	case IconRefTypeEmojiName:
		return IconRefTypeEmojiName
	default:
		return IconRefTypeInvalid // nil
	}
}

func (i IconRef) String() string {
	switch i.Type() {
	default:
		return "invalid"
	case IconRefTypeBuiltin:
		return "builtin/" + string(i[1:])
	case IconRefTypeCustom:
		return "custom/" + string(i)
	case IconRefTypeUTF8:
		if !utf8.ValidString(string(i[1:])) {
			return "invalid string"
		}
		return "utf8/" + string(i[1:])
	case IconRefTypeEmojiName:
		return "emote/" + string(i[1:])
	}
}

// Icon :D
type Icon struct {
	ID   IconID
	Name string
	Type IconType
	data []byte
}

//go:generate stringer -type=IconType -linecomment

// IconType determines the format of an icon
type IconType uint

// icon types
const (
	IconTypeUnknown IconType = iota // unknown
	IconTypeSVG                     // svg
	IconTypePNG                     // png
)

// max sizes for icon files
const (
	IconMaxSizeSVG = 2046 // bytes
	IconMaxSizePNG = 4096 // bytes
)
