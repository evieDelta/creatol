package creatol

import (
	"time"

	"codeberg.org/eviedelta/creatol/constants"
)

// LogEntry
type LogEntry struct {
	ID   AuditEntryID      // the internal ID of the Item effected
	Type constants.LogType // the primary point of action

	Details string    // details of what was done
	Time    time.Time // when it was done
	User    UserID    // and by who

	From string // initial
	Now  string // new
}
