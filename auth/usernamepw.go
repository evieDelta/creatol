package auth

import (
	"bytes"
	"context"
	"crypto/rand"
	mrand "math/rand"
	"net/http"
	"runtime"

	"codeberg.org/eviedelta/creatol"
	"codeberg.org/eviedelta/creatol/constants"
	"codeberg.org/eviedelta/creatol/wterr"
	"emperror.dev/errors"
	"github.com/jackc/pgx/v4"
	"golang.org/x/crypto/argon2"
)

func (a *Auth) NamePassRegister(ctx context.Context, ip string, useragent, displayname, login, pass string) (p creatol.UserProfile, s ClientSession, err error) {
	r, err := a.namePass.New(login, pass)
	if err != nil {
		return p, s, err
	}
	r.Kind = constants.AuthMethodUsernamePassword

	prof, err := a.newProfile(ctx, displayname, r)
	if err != nil {
		return p, s, err
	}

	auth := creatol.UserAuth{
		ID:       prof.ID,
		SourceID: r.SourceID,
		AuthType: constants.AuthMethodUsernamePassword,
		Passkey:  r.Passkey,
		Meta:     r.Meta,
	}

	err = a.DB.RegisterUser(ctx, auth, prof)
	if err != nil {
		return creatol.UserProfile{}, ClientSession{}, err
	}

	cs, err := a.newSession(ctx, newSessionIn{
		expiration: sessionExpireTime,
		ip:         ip,
		uag:        useragent,
		au:         auth,
	})
	if err != nil {
		return prof.UserProfile, cs, wterr.New(http.StatusInternalServerError).Err(err).Msg("Account successfully created but somehow we failed to create a session ( : ౦ ‸ ౦ : )").Get()
	}

	return prof.UserProfile, cs, nil
}

func (a *Auth) NamePassLogin(ctx context.Context, ip string, useragent, login, pass string) (p creatol.UserProfile, s ClientSession, err error) {
	var errMsg = "Username or Password incorrect"
	if mrand.Intn(1000) == 123 {
		errMsg = "Username or Password not incorrectn't"
	}

	auth, err := a.DB.GetUserAuthBySource(ctx, login, constants.AuthMethodUsernamePassword)
	if err != nil && errors.Is(err, pgx.ErrNoRows) {
		return p, s, wterr.New(http.StatusNotFound).Msg(errMsg).Get()
	} else if err != nil {
		return p, s, err
	}

	if !a.namePass.Validate(login, pass, auth) {
		return p, s, wterr.New(http.StatusNotFound).Msg(errMsg).Get()
	}

	pr, err := a.DB.UserProfileGetByID(ctx, auth.ID)
	if err != nil {
		return p, s, err
	}

	cs, err := a.newSession(ctx, newSessionIn{
		expiration: sessionExpireTime,
		ip:         ip,
		uag:        useragent,
		au:         auth,
	})
	if err != nil {
		return p, s, err
	}

	return pr, cs, nil
}

type UsernamePassword struct {
}

func (a *UsernamePassword) Validate(logn, pass string, db creatol.UserAuth) bool {
	k := a.Key([]byte(pass), db.Meta, logn)
	if !bytes.Equal(k, db.Passkey) {
		return false
	}

	return true
}

func (a *UsernamePassword) New(logn, pass string) (RegisterReturn, error) {
	p, err := argonParaGen()
	if err != nil {
		return RegisterReturn{}, err
	}

	hash := a.Key([]byte(pass), p, logn)

	return RegisterReturn{
		SourceID: logn,
		Passkey:  hash,
		Meta:     p,
		Kind:     constants.AuthMethodUsernamePassword,
	}, nil
}

func (a *UsernamePassword) Key(pasw []byte, para []byte, sname string) []byte {
	rsalt, t, m, s := argonParaGet(para)

	var salt = make([]byte, len(rsalt)+len(sname))
	copy(salt[:len(rsalt)], rsalt)
	copy(salt[len(sname):], sname)

	return argon2.Key(pasw, salt, t, m, uint8(runtime.NumCPU()), s)
}

const (
	// parameter indexes
	paraIndexSubType = 0

	// parameter values
	paraSubtypeArgon2 = 1
)

const (
	// parameter indexes
	argon2ParaIndexTime     = 2
	argon2ParaIndexMemory   = 3
	argon2ParaIndexSize     = 4
	argon2ParaIndexSaltHead = 8
	argon2ParaIndexSaltTail = 24

	// parameter value defaults
	argon2DefaultTime   = 3
	argon2DefaultMemory = 32
	argon2DefaultSize   = 32

	// parameter multiply
	argon2MemoryMultiply = 1024
)

func argonParaGet(para []byte) (_salt []byte, _time, _memory, _size uint32) {
	const ptim = argon2ParaIndexTime
	const pmem = argon2ParaIndexMemory
	const psiz = argon2ParaIndexSize
	const head = argon2ParaIndexSaltHead
	const tail = argon2ParaIndexSaltTail

	return para[head:tail], uint32(para[ptim]), uint32(para[pmem]) * argon2MemoryMultiply, uint32(para[psiz])
}

func argonParaGen() ([]byte, error) {
	const head = argon2ParaIndexSaltHead
	const tail = argon2ParaIndexSaltTail

	b := make([]byte, 24)
	_, err := rand.Read(b[head:tail]) // 16 bytes of pure random salt, cause why not
	if err != nil {
		return nil, err
	}

	b[paraIndexSubType] = paraSubtypeArgon2
	b[argon2ParaIndexTime] = argon2DefaultTime
	b[argon2ParaIndexMemory] = argon2DefaultMemory
	b[argon2ParaIndexSize] = argon2DefaultSize

	return b, nil
}
