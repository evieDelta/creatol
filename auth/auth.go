package auth

import (
	"context"
	"net/http"
	"time"

	"codeberg.org/eviedelta/creatol"
	"codeberg.org/eviedelta/creatol/constants"
	"codeberg.org/eviedelta/creatol/wterr"
	"emperror.dev/errors"
	"github.com/jackc/pgx/v4"
)

type AuthConfig struct {
	EnabledMethods struct {
		UsernamePassword bool
		EmailPassword    bool // not implemented
		OAuth2           bool // see providers
	}
	OAuthProviders struct {
		Discord *struct {
			Token string
			// todo
		}
	}

	InviteOnly bool

	// EncryptionKey string
}

type Auth struct {
	DB    creatol.AuthDatabase
	Media creatol.MediaFiles

	//	encryptionKey []byte

	Config AuthConfig

	namePass UsernamePassword
}

func (a *Auth) Init() error {
	//	ek, err := base64.RawStdEncoding.DecodeString(a.Config.EncryptionKey)
	//	if err != nil {
	//		return err
	//	}
	//	a.encryptionKey = ek

	return nil
}

func (a *Auth) CanRegister(method constants.AuthMethod, code string) bool {
	if a.Config.InviteOnly {
		return false // invite codes to be implemented
	}

	switch method {
	case constants.AuthMethodUsernamePassword:
		return a.Config.EnabledMethods.UsernamePassword
	case constants.AuthMethodEmailPassword:
		return false // not implemented
		return a.Config.EnabledMethods.EmailPassword
	case constants.AuthMethodOauth2:
		return false // TODO // TODO // TODO TODO TODO TODO TODO
		// this will need to also route to an enabled auth methods thing
		return a.Config.EnabledMethods.OAuth2
	default:
		return false
	}
}

/*
// TODO (maybe?)
// i decided to put off doing any encryption work here beyond hashing
// since with discord oauth as the primary authentication method we won't be
// storing anything more sensitive than the user ID
//   (which even if we don't show it you don't exactly need a data leak to deduce that via username+avatar)
//
// we don't store any oauth tokens discord gives us as we don't feel we need to
// when the only thing we use it for is authentication and syncing profile info
// both of which only need to happen when the user makes an action that'd request it
// such as, logging in, or manually requesting a profile sync
//   (and not storing it at all is one less liability in the case of a breach)
//
// however as a consideration, to future me if i end up copy pasting this for another project
// or otherwise changing the scope of this one to allow email/password or username/password
// or generally open up logins to the general public rather than only invited curators
// consider enhancing this to encrypt at rest as many things as we reasonably can
// in an effort to make the auth table as obfuscated as possible in the case of a breach
//D string
	Passkey  []byte
	Meta     []byte

	// can be ignored if not managed
	Profile ManagedProfile
}

type LoginReturn struct {
	// can be ignored if not managed
	// if managed it can be an updated profile
	Profile *ManagedProfile
}

type ManagedProfile struct {
	Name         string
	Avatar       string
	AvatarSource string
}

//   - ΔEv
*/

type RegisterReturn struct {
	//	creatol.UserAuth
	//	creatol.UserProfileMeta
	//	creatol.UserProfile

	SourceID string
	Passkey  []byte
	Meta     []byte
	Kind     constants.AuthMethod

	// can be ignored if not managed
	Profile *ManagedProfile
}

type LoginReturn struct {
	// can be ignored if not managed
	// if managed it can be an updated profile
	Profile *ManagedProfile
}

type ManagedProfile struct {
	Name         string
	Avatar       string
	AvatarSource string
}

// must be given either username or reg
func (a *Auth) newProfile(ctx context.Context, username string, reg RegisterReturn) (creatol.UserProfileMeta, error) {
	ua, err := a.DB.GetUserAuthBySource(ctx, reg.SourceID, reg.Kind)
	if err == nil && ua.SourceID == reg.SourceID {
		return creatol.UserProfileMeta{}, wterr.New(http.StatusConflict).Msg("Account already exists").Get()
	}
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		return creatol.UserProfileMeta{}, wterr.New(http.StatusInternalServerError).Err(err).Get()
	}

	id := creatol.NewUserID()

	if reg.Profile != nil {
		return creatol.UserProfileMeta{
			Managed:      true,
			AvatarSource: reg.Profile.AvatarSource,

			UserProfile: creatol.UserProfile{
				ID:     id,
				Joined: time.Now().UTC(),
				Name:   reg.Profile.Name,
			},
		}, nil
	}

	return creatol.UserProfileMeta{
		UserProfile: creatol.UserProfile{
			ID:     id,
			Joined: time.Now().UTC(),
			Name:   username,
		},
		Managed: false,
	}, nil
}
