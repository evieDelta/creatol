package auth

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"log"
	"time"

	"codeberg.org/eviedelta/creatol"
	"codeberg.org/eviedelta/creatol/internal/ratelimiter"
	"github.com/patrickmn/go-cache"
	"lukechampine.com/blake3"
)

// PruneSessions does a singular prune of expired sessions
func (a *Auth) PruneSessions(ctx context.Context) error {
	return a.DB.PruneSessions(ctx)
}

const sessionExpireTime = time.Hour * 24 * 7

// make the inputs for newSession a little less obtuse
type newSessionIn struct {
	expiration time.Duration
	ip         string
	uag        string
	au         creatol.UserAuth
}

func (a *Auth) newSession(ctx context.Context, d newSessionIn) (cs ClientSession, err error) {
	cs, st, err := generateSession(d.au)
	if err != nil {
		return cs, err
	}

	ss := creatol.UserSession{
		ID:           cs.ID,
		UserID:       d.au.ID,
		Key:          st,
		InvalidAfter: time.Now().Add(d.expiration),
		IPAddress:    d.ip,
		UserAgent:    d.uag,
	}
	cs.Expires = ss.InvalidAfter

	return cs, a.DB.NewSession(ctx, ss)
}

var sessionExpirationUpdate = ratelimiter.New(ratelimiter.Config{
	SetWhenLimited: false,
	Period:         time.Hour,
})

// might be a stupid idea
// but it'll save the amount of DB hits needed since as long as a session isn't
// invalidated the validity won't change, so as long as we remember to
// keep this up to date we can cache the result and it'll probably be fine
// the cache is set to expire in either the length until the session is invalid
// or 1 hour, whichever comes first
var validSessionCache = cache.New(time.Hour, time.Minute)

// ValidateSession determines if a session is valid or not
func (a *Auth) ValidateSession(ctx context.Context, cs ClientSession) (bool, error) {
	b, jerr := json.Marshal(cs)
	if jerr == nil {
		a, ok := validSessionCache.Get(cs.UserID.String())
		if a, ok2 := a.(string); ok && ok2 && cs.Expires.After(time.Now()) && a == base64.RawStdEncoding.EncodeToString(b) { // because the expired crapper
			return true, nil
		}
	} else {
		log.Println(jerr)
	}

	key := sessionKey(cs)
	ses, au, err := a.DB.GetSession(ctx, key)
	if err != nil {
		return false, err
	}

	ok := compareSession(cs, au, ses)
	if !ok {
		return ok, nil
	}

	if jerr == nil {
		e := cs.Expires.Sub(time.Now())
		if e > time.Hour {
			e = time.Hour
		}

		if e > time.Second {
			validSessionCache.Add(cs.UserID.String(), base64.RawStdEncoding.EncodeToString(b), e)
		}
	}

	if sessionExpirationUpdate.Do(cs.ID.String()) {
		err = a.DB.UpdateSession(ctx, cs.ID, time.Now().UTC().Add(sessionExpireTime), time.Now().UTC())
		if err != nil {
			return ok, err
		}
	}

	return ok, nil
}

func (a *Auth) InvalidateSessionID(ctx context.Context, id creatol.SessionID) error {
	validSessionCache.Delete(id.String())

	return a.DB.InvalidateSession(ctx, id)
}

func (a *Auth) InvalidateSessionToken(ctx context.Context, key []byte) error {
	id, err := a.DB.InvalidateSessionKey(ctx, key)
	if err != nil {
		return err
	}

	validSessionCache.Delete(id.String())
	return nil
}

// generate session takes a userAuth object and generates a session, returning a session object for the client, and a key to be stored on the server
func generateSession(au creatol.UserAuth) (user ClientSession, store []byte, err error) {
	b := make([]byte, 32)
	_, err = rand.Read(b)
	if err != nil {
		return user, nil, err
	}

	user = ClientSession{Key: b, Created: time.Now().UTC(), UserID: au.ID, ID: creatol.NewSessionID()}
	store = sessionKey(user)
	user.Hash = sessionHash(user, store, au)

	return user, store, nil
}

type ClientSession struct {
	ID      creatol.SessionID `json:"id"`
	Key     []byte            `json:"k"`
	UserID  creatol.UserID    `json:"uid"`
	Created time.Time         `json:"cd"`
	Expires time.Time         `json:"e"`
	Hash    []byte            `json:"h"` // equal to key + serverSession.Key + auth.(SourceID + Passkey + Meta) > blake3
}

// takes a client session, and generates the rest session token from it
// the return result is used as a key on the server for fetching sessions
func sessionKey(cs ClientSession) []byte {
	base := make([]byte, 0, len(cs.Key)+len(cs.UserID.Base16())+len(cs.Created.Format(time.RFC3339)))
	base = append(base, cs.Key...)
	base = append(base, cs.UserID.Base16()...)
	base = append(base, cs.Created.Format(time.RFC3339)...)

	h := blake3.Sum512(base)

	return h[:]
}

func joinBytes(b ...[]byte) []byte {
	l := 0
	for _, x := range b {
		l += len(x)
	}
	c := make([]byte, 0, l)
	for _, x := range b {
		c = append(c, x...)
	}
	return c
}

// generates an extended hash of a session, using the client session, the at rest hash, and the users auth info
// to cryptographically ensure that sessions are not considered valid if a user updates their auth information
// the result is compared against the hash value of a client session
func sessionHash(cs ClientSession, ds []byte, au creatol.UserAuth) []byte {
	base := joinBytes(cs.Key, ds, []byte(au.SourceID), au.Passkey, au.Meta)
	h := blake3.Sum512(base)

	return h[:]
}

// validate a session and make sure all is well
func compareSession(cs ClientSession, au creatol.UserAuth, ref creatol.UserSession) bool {
	ucom := sessionHash(cs, ref.Key, au)
	return bytes.Equal(ucom, []byte(cs.Hash)) &&
		cs.UserID == ref.UserID && au.ID == ref.UserID &&
		ref.InvalidAfter.After(time.Now()) &&
		ref.LastUsed.Before(time.Now())
}
