package constants

//go:generate stringer -type=TagRating -linecomment

// TagRating is used for rating how well a tag applies to a maker
type TagRating uint8

// Rating constants
// Undefined: not yet specified
// 0 Star   : not applicable at all
// 1 Star   : technically applicable but not executed well enough to really count
// 2 Star   : applicable but maybe not ideal but at least satisfactory
// 3 Star   : applicable and done really well
const (
	RatingUndefined TagRating = 0 // unspecified
	RatingUnrated   TagRating = 0

	Rating0Star TagRating = 1 // 0 star, n/a
	Rating1Star TagRating = 2 // 1 star, hardly
	Rating2Star TagRating = 3 // 2 star, satisfactory
	Rating3Star TagRating = 4 // 3 star, great

	// aliases
	RatingNone      TagRating = 1 // 0 star, not applicable at all
	RatingZero      TagRating = 1
	RatingZeroStar  TagRating = 1
	RatingBarely    TagRating = 2 // 1 star, technically applicable but not executed well enough to really count
	RatingOne       TagRating = 2
	RatingOneStar   TagRating = 2
	RatingPresent   TagRating = 3 // 2 star, applicable and maybe not ideal but at least satisfactory
	RatingTwo       TagRating = 3
	RatingTwoStar   TagRating = 3
	RatingGood      TagRating = 4 // 3 star, applicable and does it really well
	RatingThree     TagRating = 4
	RatingThreeStar TagRating = 4

	Rating0 TagRating = 1
	Rating1 TagRating = 2
	Rating2 TagRating = 3
	Rating3 TagRating = 4
)

var tagRatingTooltips = map[TagRating]string{
	RatingUndefined: "No rating has been assigned yet",
	Rating0Star:     "0 Stars: Not applicable",
	Rating1Star:     "1 Stars: Technically applicable but not executed well enough to really count",
	Rating2Star:     "2 Stars: Not necessarily ideal but applicable and at least satisfactorily so",
	Rating3Star:     "3 Stars: Applicable and well executed",
}
