// Code generated by "stringer -type=AuthMethod -linecomment"; DO NOT EDIT.

package constants

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[AuthMethodUnknown-0]
	_ = x[AuthMethodUsernamePassword-1]
	_ = x[AuthMethodEmailPassword-2]
	_ = x[AuthMethodOauth2-64]
}

const (
	_AuthMethod_name_0 = "Unknown or UnregisteredUsername and PasswordEmail and Password"
	_AuthMethod_name_1 = "Oauth2"
)

var (
	_AuthMethod_index_0 = [...]uint8{0, 23, 44, 62}
)

func (i AuthMethod) String() string {
	switch {
	case 0 <= i && i <= 2:
		return _AuthMethod_name_0[_AuthMethod_index_0[i]:_AuthMethod_index_0[i+1]]
	case i == 64:
		return _AuthMethod_name_1
	default:
		return "AuthMethod(" + strconv.FormatInt(int64(i), 10) + ")"
	}
}
