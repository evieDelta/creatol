// Package constants contains constants for the creatol API
/*
these may be used both internally and externally, they may be stored in a database and be sent to external systems,
many of these will get stored in a database as is, and some may potentially be sent over a public API.

as such when editing care should be taken as to not mess up the meaning of existing values, especially in the constants that make use of iota,
as doing so may hamper the function of external clients, or cause data errors in the database if not migrated.

if the x_string.go file gives you an error you'll know one of the constant values was modified accidentially
*/
package constants
