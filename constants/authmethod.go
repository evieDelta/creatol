package constants

//go:generate stringer -type=AuthMethod -linecomment

// AuthMethod is used for describing how a user is authenticated
// this is not revealed on the public API
type AuthMethod uint8

// Auth methods
// values greater than 100 are special login methods (such as oauth or oidc) while values lesser are simple login methods, such as username and password via bcrypt
const (
	AuthMethodUnknown          AuthMethod = 0                    // Unknown or Unregistered
	AuthMethodUsernamePassword AuthMethod = 1                    // Username and Password
	AuthMethodEmailPassword    AuthMethod = 2                    // Email and Password
	AuthMethodOauth2           AuthMethod = 64                   // Oauth2
	AuthMethodOauth2Discord               = AuthMethodOauth2 | 1 // Oauth2 Discord
)
