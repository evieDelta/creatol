package constants

//go:generate stringer -type=MarkupType -linecomment

// MarkupType is the markup language used to format a page
type MarkupType uint8

// Markup types
const (
	MarkupTypePlain    MarkupType = iota // plain-text
	MarkupTypeGemtext                    // gemtext
	MarkupTypeMarkdown                   // markdown

	// probably not gonna implement these

	_ // MarkupTypeAsciiDoc         // asciidoc
	_ // MarkupTypeReStructuredText // rst/restructuredtext
	_ // MarkupTypeBBCode           // bbcode
	_ // MarkupTypeHTML             // html
	_ // MarkupTypeWikiCreole       // wiki-creole
	_ // MarkupTypeMediaWiki        // mediawiki
)
