package constants

//go:generate stringer -type=MakerType -linecomment

// MakerType determines the type of a maker
type MakerType uint

// Maker types
// Other   : Undefined or Unspecified, most likely won't be seen in normal use
// Website : Any websites that do not fit into other defined categories
// App     : Programs that have to be downloaded and ran on a computer or mobile device
const (
	MakerTypeOther    MakerType = 0 // unspecified/undefined
	MakerTypeWebsites MakerType = 1 // other-websites
	MakerTypeApps     MakerType = 2 // downloadable-apps
	MakerTypePicrew   MakerType = 3 // picrews
)
