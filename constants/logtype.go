package constants

//go:generate stringer -type=LogType -linecomment

type LogType uint

// Log entry types
const (
	LogTypeUnknown LogType = 0 // unknown

	// tag log
	LogTypeTagMain       LogType = iota + 100 // tag-other
	LogTypeTagName                            // tag-name
	LogTypeTagAliases                         // tag-aliases
	LogTypeTagDecription                      // tag-description
)
