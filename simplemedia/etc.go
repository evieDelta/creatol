package simplemedia

import (
	// its embed, why else _gopls_
	_ "embed"

	"bytes"
	"encoding/base32"
	"hash"
	"image"
	"image/png"
	"io"
	"strings"

	"lukechampine.com/blake3"
)

// B32Set is the base32 alphabet used for encoding names
// https://en.wikipedia.org/wiki/Base32#Word-safe_alphabet
const B32Set = "23456789CFGHJMPQRVWXcfghjmpqrvwx"

// B32 is the base32 encoding used for names
var B32 = base32.NewEncoding(B32Set).WithPadding(base32.NoPadding)

func genFilenameReader(r io.Reader) string {
	h := blake3.New(32, nil)
	io.Copy(h, r)

	hash := h.Sum(make([]byte, 0, 32))
	name := B32.EncodeToString(hash)

	return name
}

func genFilenameHash(h hash.Hash) string {
	hash := h.Sum(make([]byte, 32))
	name := B32.EncodeToString(hash)

	return name
}

func genFilename(b []byte) string {
	// generate a hash and Base32 encode it (using a customish Base32)
	hash := blake3.Sum256(b)
	name := B32.EncodeToString(hash[:])

	return name
}

func scaleProportionally(width, height int, biggestto int, scaleUp bool) (w2, h2 int) {
	if !scaleUp && width <= biggestto && height <= biggestto {
		return width, height
	}
	if width == height {
		return biggestto, biggestto
	}

	fw, fh, max := float64(width), float64(height), float64(biggestto)

	if scaleUp {
		if fw > fh {
			return biggestto, int(fh / (max / fw))
		}

		return int(fw / (max / fh)), biggestto
	}

	if fw > fh {
		return biggestto, int(fh / (fw / max))
	}

	return int(fw / (fh / max)), biggestto
}

// returns an image extension from a mime type
func extensionFromMime(mimet string) string {
	// fallback so if its not a mimetype it'll return as is
	if !strings.Contains(mimet, "/") {
		return mimet
	}
	switch strings.ToLower(mimet) {
	case "image/png":
		return "png"
	case "image/jpeg":
		return "jpeg"
	case "image/webp":
		return "webp"
	case "image/gif":
		return "gif"
	case "image/tiff":
		return "tiff"
	default:
		return "dat" // shouldn't ever happen, but just in case
	}
}

// icon for gifs

//go:embed gifIcon.png
var gifIcon []byte

var gifIconImg image.Image

func init() {
	img, err := png.Decode(bytes.NewReader(gifIcon))
	if err != nil {
		panic(err)
	}
	gifIconImg = img
}
