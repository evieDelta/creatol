package simplemedia

import (
	"encoding/json"
	"errors"

	"go.etcd.io/bbolt"
)

type db struct {
	B *bbolt.DB
}

var (
	bucMeta   = []byte("meta")
	bucImages = []byte("images")
)

var (
	keyMetaVersion          = []byte("version")
	keyMetaImageBuildThumb  = []byte("image-build-thumb")
	keyMetaImageBuildNormal = []byte("image-build-normal")
)

func (db *db) Open(location string) (err error) {
	db.B, err = bbolt.Open(location, 0o0660, nil)
	if err != nil {
		return err
	}

	return db.Init()
}

func (db *db) PutImage(name string, i imageMeta) error {
	return db.B.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket(bucImages)
		if b == nil {
			return errors.New("invalid DB (PutImage)")
		}

		dat, err := json.Marshal(i)
		if err != nil {
			return err
		}

		err = b.Put([]byte(name), dat)
		if err != nil {
			return err
		}

		return nil
	})
}

func (db *db) GetImage(name string) (i imageMeta, err error) {
	err = db.B.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket(bucImages)
		if b == nil {
			return errors.New("invalid DB (GetImage)")
		}

		dat := b.Get([]byte(name))
		if dat == nil {
			return errors.New("unknown image")
		}

		return json.Unmarshal(dat, &i)
	})

	return i, err
}

func (db *db) DelImage(name string) error {
	return db.B.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket(bucImages)
		if b == nil {
			return errors.New("invalid DB (DelImage)")
		}

		err := b.Delete([]byte(name))
		if err != nil {
			return err
		}

		return nil
	})
}

func (db *db) Init() error {
	// basic initialisation
	err := db.B.Update(func(tx *bbolt.Tx) error {
		if tx.Bucket(bucMeta) != nil {
			return nil
		}

		meta, err := tx.CreateBucketIfNotExists(bucMeta)
		if err != nil {
			return err
		}
		_, err = tx.CreateBucketIfNotExists(bucImages)
		if err != nil {
			return err
		}
		err = meta.Put(keyMetaVersion, []byte{1})
		if err != nil {
			return err
		}

		return nil
	})

	return err
}

// Change detection

// HasConfigChangedNormal detects whether or not the config for normal images has changed
// so it can tell whether or not it needs to rebuild the normal image directory
func (db *db) HasConfigChangedNormal(c config) (changed bool, err error) {
	// Normal Image data
	nstr := c.Normals.String()

	// Fetch and compare normal image settings
	err = db.B.Update(func(tx *bbolt.Tx) error {
		var key = keyMetaImageBuildNormal

		meta := tx.Bucket(bucMeta)
		if meta == nil {
			return errors.New("invalid DB (hasConfigChangedNormal)")
		}
		odat := meta.Get(key)
		if odat == nil {
			err = meta.Put(key, []byte(nstr))
			if err != nil {
				return err
			}

			return nil
		}

		changed = string(odat) != nstr

		if !changed {
			return nil
		}

		err = meta.Put(key, []byte(nstr))
		if err != nil {
			return err
		}

		return nil
	})

	return changed, err
}

// HasConfigChangedThumb detects whether or not the config for thumbnail images has changed
// so it can tell whether or not it needs to rebuild the thumbnail image directory
func (db *db) HasConfigChangedThumb(c config) (changed bool, err error) {
	// Thumb Image data
	nstr := c.Thumbnails.String()

	// Fetch and compare thumbnail image settings
	err = db.B.Update(func(tx *bbolt.Tx) error {
		var key = keyMetaImageBuildThumb

		meta := tx.Bucket(bucMeta)
		if meta == nil {
			return errors.New("Invalid DB (hasConfigChangedThumb)")
		}
		odat := meta.Get(key)
		if odat == nil {
			err = meta.Put(key, []byte(nstr))
			if err != nil {
				return err
			}

			return nil
		}

		changed = string(odat) != nstr

		if !changed {
			return nil
		}

		err = meta.Put(key, []byte(nstr))
		if err != nil {
			return err
		}

		return nil
	})

	return changed, err
}
