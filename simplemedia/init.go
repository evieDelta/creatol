package simplemedia

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
	"syscall"

	"github.com/pelletier/go-toml/v2"
)

const dirPermissions fs.FileMode = 0o0775 | fs.ModeDir
const permissions fs.FileMode = 0o0664

func (h *Handler) Initialise(conf string) error {
	err := toml.Unmarshal([]byte(defaultConfig), &h.Config)
	if err != nil {
		fmt.Println("Error in SimpleMediaHandler defaults")
		panic(err) // if the default config has something wrong, there is something wrong, so to make it clear its an issue in the library itself and not in a users config we panic
	}
	err = toml.Unmarshal([]byte(conf), &h.Config)
	if err != nil {
		return err
	}

	// if the serve directory doesn't exist we create it
	if _, err := os.Lstat(filepath.Join(h.Config.Locations.ServeDirectory, dirOriginal)); os.IsNotExist(err) {
		err = os.Mkdir(h.Config.Locations.ServeDirectory, dirPermissions)
		if err != nil {
			return err
		}
		err = os.Mkdir(filepath.Join(h.Config.Locations.ServeDirectory, dirOriginal), dirPermissions)
		if err != nil {
			return err
		}
		err = os.Mkdir(filepath.Join(h.Config.Locations.ServeDirectory, dirNormal), dirPermissions)
		if err != nil {
			return err
		}
		err = os.Mkdir(filepath.Join(h.Config.Locations.ServeDirectory, dirThumbnails), dirPermissions)
		if err != nil {
			return err
		}
	}

	// open the database
	err = h.DB.Open(h.Config.Locations.DatabaseLocation)
	if err != nil {
		return err
	}

	// check if there is a change to the config (happens in the below func)
	// and rebuild the thumbnail and normal dirs if so
	err = h.rebuildImageDir()
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) rebuildImageDir() (err error) {
	// check if the configs have changed and are in need of rebuilding
	{
		// i created these functions with the intent that it could rebuild one or the other
		// but then i realised my actual building functions wont work with that, so i just
		// merge the change booleans together and treat them as the same
		// at least keeping the config references separate allows the possibility of future
		// improvement allowing it to rebuild thumbnails but not the normal images or vice versa
		normal, err := h.DB.HasConfigChangedNormal(h.Config)
		if err != nil {
			return err
		}
		thumb, err := h.DB.HasConfigChangedThumb(h.Config)
		if err != nil {
			return err
		}
		changed := thumb || normal
		if !changed {
			return nil
		}
	}

	temp := filepath.Join(h.Config.Locations.ServeDirectory, ".rebuild-cache")
	// make a tempdir for putting the new build in
	err = os.Mkdir(temp, dirPermissions)
	if err != nil {
		return err
	}

	// not actually really a lockfile, but just verify we can write to the directories
	tf, err := os.OpenFile(filepath.Join(h.Config.Locations.ServeDirectory, ".lockfile"), os.O_EXCL|os.O_CREATE, 0o0644)
	if err != nil {
		os.RemoveAll(temp)
		return err
	}

	var success bool
	defer func() {
		// remove the lockfile last
		defer func() {
			tf.Close()
			err = os.Remove(filepath.Join(h.Config.Locations.ServeDirectory, ".lockfile"))
			if err != nil {
				log.Println("failed to remove .lockfile", err)
			}
			err = os.Remove(temp)
			if err != nil {
				log.Println("failed to remove rebuild cache dir", err)
			}
		}()

		// if it didn't succeed remove the rebuild dir
		if !success {
			err = os.RemoveAll(temp)
			log.Println("rebuild failed")
			return
		}

		// Finalise New thumbnails by deleting the old ones and putting the new ones in place
		{
			var dir = filepath.Join(h.Config.Locations.ServeDirectory, dirThumbnails)
			var new = filepath.Join(temp, dirThumbnails)

			err = os.RemoveAll(dir)
			if err != nil {
				log.Fatalln("error removing", dir, " (thumbnails) |", err)
			}
			err = syscall.Rename(new, dir)
			if err != nil {
				log.Fatalln("error moving new thumbnail build to final location |", err)
			}
		}

		// Finalise New normal images by deleting the old ones and putting the new ones in place
		{
			var dir = filepath.Join(h.Config.Locations.ServeDirectory, dirNormal)
			var new = filepath.Join(temp, dirNormal)

			err = os.RemoveAll(dir)
			if err != nil {
				log.Fatalln("error removing", dir, " (normal images) |", err)
			}
			err = os.Rename(new, dir)
			if err != nil {
				log.Fatalln("error moving new normal image build to final location |", err)
			}
		}
	}()

	// some file paths we'll need for the build
	var dirori = filepath.Join(h.Config.Locations.ServeDirectory, dirOriginal)

	var tmpthumb = filepath.Join(temp, dirThumbnails)
	var tmpimage = filepath.Join(temp, dirNormal)

	// create the temp directories for the new build
	err = os.Mkdir(tmpthumb, dirPermissions)
	if err != nil {
		return err
	}
	err = os.Mkdir(tmpimage, dirPermissions)
	if err != nil {
		return err
	}

	// open the directory containing the originals
	dir, err := os.ReadDir(dirori)
	if err != nil {
		return err
	}

	// go through all of them
	for _, x := range dir {
		if x.IsDir() {
			continue
		}
		f, err := os.Open(filepath.Join(dirori, x.Name()))
		if err != nil {
			return err
		}

		// get the name and the format from the filename
		var name = x.Name()
		var format = ""
		if y := strings.SplitN(x.Name(), ".", 2); len(y) >= 2 {
			name = y[0]
			format = y[1]
		}

		thumb, normal, err := h.HandleImage(f, format)
		if err != nil {
			return err
		}

		err = os.WriteFile(filepath.Join(tmpthumb, name+"."+h.Config.Thumbnails.Format), thumb, permissions)
		if err != nil {
			return err
		}
		err = os.WriteFile(filepath.Join(tmpimage, name+"."+h.Config.Normals.Format), normal, permissions)
		if err != nil {
			return err
		}

		if h.Config.Debug.LogRebuild {
			log.Println("built", name)
		}
	}

	success = true

	if h.Config.Debug.LogRebuild {
		log.Println("done rebuilding")
	}

	return nil
}
