package simplemedia

import (
	"bytes"
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"strings"

	"github.com/disintegration/imaging"
	"github.com/muesli/smartcrop"
	"golang.org/x/image/tiff"
)

// formatStatic formats the normal version and a thumbnail of a static image
func (h *Handler) formatStatic(img image.Image) (thumbnail, normal []byte, err error) {
	// generate a thumbnail
	thumbnail, err = h.formatThumbnail(img)
	if err != nil {
		return nil, nil, err
	}

	// generate the size to scale it to
	wi, hi := scaleProportionally(
		img.Bounds().Dx(), img.Bounds().Dy(),
		h.Config.Normals.Resolution,
		false,
	)

	// resize it
	img = imaging.Resize(img, wi, hi, imaging.Lanczos)

	// encode the image to the determined format
	normal, err = encodeImg(img, h.Config.Normals.Format, h.Config.Normals.Quality)
	if err != nil {
		return nil, nil, err
	}

	return
}

type resizer struct{}

func (resizer) Resize(img image.Image, width, height uint) image.Image {
	return imaging.Resize(img, int(width), int(height), imaging.Lanczos)
}

var smortcropper = smartcrop.NewAnalyzer(new(resizer))

// formatThumbnail formats a thumbnail of an image
func (h *Handler) formatThumbnail(img image.Image) (thumbnail []byte, err error) {

	// check whether or not to crop the thumbnail
	if h.Config.Thumbnails.Crop {
		// works i guess, too lazy to do this myself
		// if it states to crop it square, just use the function from imaging, idc to figure out how to do it myself
		//img = imaging.Thumbnail(img, h.Config.Thumbnails.Resolution, h.Config.Thumbnails.Resolution, imaging.Lanczos)

		crap, err := smortcropper.FindBestCrop(img, h.Config.Thumbnails.Resolution, h.Config.Thumbnails.Resolution)
		if err != nil {
			return nil, err
		}
		type SubImager interface {
			SubImage(r image.Rectangle) image.Image
		}
		img = img.(SubImager).SubImage(crap)

		img = imaging.Resize(img, h.Config.Thumbnails.Resolution, h.Config.Thumbnails.Resolution, imaging.Lanczos)

	} else {
		// generate the new image resolution
		wi, hi := scaleProportionally(
			img.Bounds().Dx(), img.Bounds().Dy(),
			h.Config.Thumbnails.Resolution,
			h.Config.Thumbnails.ScaleUp,
		)

		// scale it
		img = imaging.Resize(img, wi, hi, imaging.Lanczos)
	}

	return encodeImg(img, h.Config.Thumbnails.Format, h.Config.Thumbnails.Quality)
}

func (h *Handler) formatGif(GIF *gif.GIF) (thumbnail, normal []byte, err error) {
	if len(GIF.Image) == 0 {
		return nil, nil, errors.New("No images in gif")
	}

	// if there is only 1 frame in the gif treat it the same as a static image
	if len(GIF.Image) == 1 {
		return h.formatStatic(GIF.Image[0])
	}

	img := image.Image(GIF.Image[0])

	// generate a thumbnail
	thumbnail, err = h.formatThumbnail(img)
	if err != nil {
		return nil, nil, err
	}

	// create a static image scaled the same as a normal static image
	wi, hi := scaleProportionally(img.Bounds().Dx(), img.Bounds().Dy(), h.Config.Normals.Resolution, false)

	img = imaging.Resize(img, wi, hi, imaging.Lanczos)

	// create a gif icon scaled per the image resolution
	gicon := imaging.Resize(
		gifIconImg,
		gifIconImg.Bounds().Dx()*(img.Bounds().Dx()/150), 0,
		imaging.NearestNeighbor,
	)

	// paste the gif icon in
	img = imaging.Overlay(
		img, gicon,
		img.Bounds().Size().Sub(gicon.Bounds().Size()),
		75,
	)

	// encode the end result for the normal image
	normal, err = encodeImg(img, h.Config.Normals.Format, h.Config.Normals.Quality)
	if err != nil {
		return nil, nil, err
	}

	return thumbnail, normal, nil
}

// quality is only used for lossy formats (eg jpeg)
// supported formats are png and jpeg/jpg
func encodeImg(img image.Image, format string, quality int) (dat []byte, err error) {
	b := bytes.NewBuffer(dat)

	// only supports png and jpeg as they are the only supported formats worth encoding to that i have an encoder for

	// .bmp  is useless and inefficient nowadays

	// .tiff is rarely supported and doesn't have much utility not covered by other formats outside of professional camera stuff anyways

	// .gif  isn't really that good especially for static images
	//      (and every single image displaying thing in the past 2 decades supports at least png or jpeg anyways
	//       if something you use doesn't support .png or .jpeg and requires you to use .gif why are you using it)

	// .ff   why
	//      (farbfeld is a neat format in theory for its simplicity, but in practice its kinda useless and not supported anywhere
	//       and go doesn't have a bzip2 encoder which seems to be the standard for compressing it)

	// .webp if golang.org/x/image/webp supported encoding i would include webp as an option, but it doesn't
	switch strings.ToLower(format) {
	case "png":
		err = png.Encode(b, img)
	case "jpeg", "jpg":
		err = jpeg.Encode(b, img, &jpeg.Options{
			Quality: quality,
		})
	case "webp":
		err = webpEncode(b, img, quality)
	}

	return b.Bytes(), err
}

// HandleImage takes an input image and decodes it to generate a thumbnail and a normalised version
// format can either be the extension (without the leading .) or a mime type
func (h *Handler) HandleImage(datr io.Reader, format string) (thumb, normal []byte, err error) {
	var img image.Image
	var agf *gif.GIF

	// determine what format to decode from
	switch strings.ToLower(format) {
	case "image/png", "png":
		img, err = png.Decode(datr)
	case "image/jpeg", "jpeg", "jpg":
		img, err = jpeg.Decode(datr)
	case "image/webp", "webp":
		img, err = webpDecode(datr)
	case "image/gif", "gif":
		agf, err = gif.DecodeAll(datr)
	case "image/tiff", "tiff":
		img, err = tiff.Decode(datr)

	// might as well try this, though the above probably cover all of them already
	default:
		img, format, err = image.Decode(datr)
	}

	if err != nil {
		return nil, nil, err
	}

	// use the gif handler if its a gif, else use the static handler
	if agf != nil {
		thumb, normal, err = h.formatGif(agf)
	} else {
		thumb, normal, err = h.formatStatic(img)
	}

	return thumb, normal, err
}
