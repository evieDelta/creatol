package simplemedia

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"

	"codeberg.org/eviedelta/creatol"
)

const (
	dirOriginal   = "original"
	dirNormal     = "image"
	dirThumbnails = "thumbnail"
)

type imageMeta struct {
	Original string // original is the given name of the original file placed in the original folder (including its extension)
}

// New returns a new instance of a Simple Media Handler
func New() creatol.MediaFiles {
	return &Handler{}
}

// Handler is a simpler media handler
type Handler struct {
	Config config
	DB     db
}

func (h *Handler) GetDefaultConfig() string {
	return defaultConfig
}

// ImgStore takes image data and a mime type
// format can either be the extension (without the leading .) or a mime type
func (h *Handler) ImgStore(data io.Reader, format string) (name string, err error) {
	var rsk io.ReadSeeker
	var tmp *os.File

	if r, ok := data.(io.ReadSeeker); ok && !h.Config.Metadata.UseEXIFTool && !h.Config.Metadata.UseEXIV2 {
		// if the file is a readseeker we don't need to cache it on our own, we can just seek back
		// unless we are using exiftool, in which case we are going to save it anyways so we can pass it through exiftool
		rsk = r
	} else {
		tmp, err = os.CreateTemp("", "swid-cache-file-*-"+format+".dat")
		if err != nil {
			return "", err
		}
		// make sure we remove the file afterwards
		defer func() {
			if tmp == nil {
				return
			}
			err := tmp.Close()
			if err != nil {
				log.Println("error closing temp file (fin):", err)
			}
			err = os.Remove(tmp.Name())
			if err != nil {
				log.Println("error removing temp file:", err)
			}
		}()

		// copy the reader to it
		_, err = io.Copy(tmp, data)
		if err != nil {
			return "", err
		}

		n := tmp.Name()

		if h.Config.Metadata.UseEXIV2 {
			err = tmp.Close()
			if err != nil {
				log.Println("error closing temp file (mid):", err)
			}
			tmp = nil

			e := exec.Command("exiv2", "-d", "a", n)
			ret, err := e.CombinedOutput()
			if err != nil {
				return "", fmt.Errorf("%v: %v", err, string(ret))
			}
		}

		if h.Config.Metadata.UseEXIFTool && (format != "image/webp" && format != "webp") {
			err = tmp.Close()
			if err != nil {
				log.Println("error closing temp file (mid):", err)
			}
			tmp = nil

			e := exec.Command("exiftool", "-all=", "--", n)
			ret, err := e.CombinedOutput()
			if err != nil {
				return "", fmt.Errorf("%v: %v", err, string(ret))
			}

			// don't want to keep the backup with metadata
			defer func() {
				if _, err := os.Lstat(n + "_original"); os.IsNotExist(err) {
					return
				}
				err = os.Remove(n + "_original")
				if err != nil {
					log.Println("error removing exiftool _original backup", err)
				}
			}()
		}

		tmp, err = os.Open(n)
		if err != nil {
			return "", err
		}

		_, err = tmp.Seek(0, io.SeekStart)
		if err != nil {
			return "", err
		}

		rsk = tmp
	}

	// generate the name hash and final names
	var dir = h.Config.Locations.ServeDirectory
	name = genFilenameReader(rsk)

	ogn := filepath.Join(dir, dirOriginal, name+"."+extensionFromMime(format))
	thn := filepath.Join(dir, dirThumbnails, name+"."+h.Config.Thumbnails.Format)
	nmn := filepath.Join(dir, dirNormal, name+"."+h.Config.Normals.Format)

	//	if func() bool {
	//		_, err := os.Stat(ogn)
	//		if err != nil && !os.IsNotExist(err) {
	//			return false
	//		}
	//		return true
	//	}() == true {
	//		return "", nil
	//	}

	// check if a file already exists (we can do this because the hash name scheme provides inherit deduplication)
	if _, err := os.Stat(ogn); err == nil || !os.IsNotExist(err) {
		return "", errors.New("image already exists")
	}

	// generate the thumbnail and normal versions
	_, err = rsk.Seek(0, io.SeekStart)
	if err != nil {
		return "", err
	}

	thumb, normal, err := h.HandleImage(rsk, format)
	if err != nil {
		return "", err
	}

	// Write the original file into the original directory
	{
		// reset to the beginning
		_, err = rsk.Seek(0, io.SeekStart)
		if err != nil {
			return "", err
		}

		// create a file in the final destination
		var f *os.File
		f, err = os.OpenFile(ogn, os.O_CREATE|os.O_RDWR, permissions)
		if err != nil {
			return "", err
		}

		// and copy the data to it
		_, err = io.Copy(f, rsk)
		if err != nil {
			return "", err
		}
	}

	// write the thumb and normal versions
	err = os.WriteFile(thn, thumb, permissions)
	if err != nil {
		return "", err
	}
	err = os.WriteFile(nmn, normal, permissions)
	if err != nil {
		return "", err
	}

	return name, h.DB.PutImage(name, imageMeta{
		Original: name + "." + extensionFromMime(format),
	})
}

// ImgDelete deletes an image
func (h *Handler) ImgDelete(name string) error {
	var dir = h.Config.Locations.ServeDirectory

	meta, err := h.DB.GetImage(name)
	if err != nil {
		return err
	}

	files := []string{
		filepath.Join(dir, dirOriginal, meta.Original),
		filepath.Join(dir, dirThumbnails, name+"."+h.Config.Thumbnails.Format),
		filepath.Join(dir, dirNormal, name+"."+h.Config.Normals.Format),
	}

	for _, x := range files {
		err = os.Remove(x)
		if err != nil {
			return err
		}
	}

	return h.DB.DelImage(name)
}

func (h *Handler) EndpointImgNormal(name string) string {
	return filepath.Join(dirNormal, name+"."+h.Config.Normals.Format)
}

func (h *Handler) EndpointImgOriginal(name string) string {
	meta, err := h.DB.GetImage(name)
	if err != nil {
		log.Println("simple media: db entry not found for", name, " | ", err)
		return name + ".png?err=\"unknown_file\"" // can just guess ig
	}
	return path.Join(dirOriginal, meta.Original)
}

func (h *Handler) EndpointImgThumbnail(name string) string {
	return path.Join(dirThumbnails, name+"."+h.Config.Thumbnails.Format)
}
