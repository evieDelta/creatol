package simplemedia

import (
	"fmt"
	"strings"

	// its the embed package, why else would i be importing it
	_ "embed"
)

//go:embed defaultconfig.toml
var defaultConfig string

type config struct {
	Locations struct {
		// Public directory
		ServeDirectory string `toml:"ServeDirectory"`
		// Database location
		DatabaseLocation string `toml:"DatabaseLocation"`
	} `toml:"Locations"`

	Metadata struct {
		// use <https://exiftool.org/> to remove exif metadata from the original image
		// does not work with webp images currently
		UseEXIFTool bool `toml:"UseEXIFTool"`
		// use <https://www.exiv2.org/> to remove exif metadata from the original image
		UseEXIV2 bool `toml:"UseEXIV2"`
	} `toml:"Metadata"`

	Thumbnails thumbnails `toml:"Thumbnails"`

	Normals normals `toml:"Normals"`

	Debug struct {
		// Log the rebuild process
		LogRebuild bool `toml:"LogRebuild"`
	} `toml:"Debug"`
}

type normals struct {
	Format     string `toml:"Format"`
	Quality    int    `toml:"Quality"`
	Resolution int    `toml:"Resolution"`
}

func (c normals) String() string {
	// boilerplate
	b := &strings.Builder{}
	w := func(s interface{}) {
		b.WriteString(fmt.Sprint(s))
		b.WriteRune('\t')
	}

	// format values
	w(c.Format)
	w(c.Quality)

	// size values
	w(c.Resolution)

	return b.String()
}

type thumbnails struct {
	Format     string `toml:"Format"`
	Quality    int    `toml:"Quality"`
	Resolution int    `toml:"Resolution"`

	Crop    bool `toml:"Crop"`
	ScaleUp bool `toml:"ScaleUp"` // currently broken, can't be bothered to figure out why atm
}

func (c thumbnails) String() string {
	// boilerplate
	b := &strings.Builder{}
	w := func(s interface{}) {
		b.WriteString(fmt.Sprint(s))
		b.WriteRune('\t')
	}

	// format values
	w(c.Format)
	w(c.Quality)

	// size values
	w(c.Crop)
	w(c.ScaleUp)
	w(c.Resolution)

	return b.String()
}
