package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"codeberg.org/eviedelta/creatol/simplemedia"
)

func main() {
	sm := simplemedia.New()

	dat, err := os.ReadFile("./config.toml")
	if err != nil {
		log.Println(err)
		if os.IsNotExist(err) {
			log.Println(os.WriteFile("./config.toml", []byte(sm.GetDefaultConfig()), 0664))
			log.Println("wrote config")
		}
		return
	}

	err = sm.Initialise(string(dat))
	if err != nil {
		log.Fatal("sm.Initialise", err)
	}

	err = os.Mkdir("./waiting", 0o0664)
	if err != nil && !os.IsExist(err) {
		log.Fatal("mkdir", err)
	}

	go func() {
		http.HandleFunc("/index", func(w http.ResponseWriter, r *http.Request) {
			f, err := os.ReadDir("./public/thumbnail")
			if err != nil {
				log.Println(err)
				return
			}

			var files = []string{}

			for _, x := range f {
				files = append(files, strings.Split(x.Name(), ".")[0])
			}

			fmt.Fprintln(w, `
<!DOCTYPE html>
<html>
<head>
<style>
.hv {
	display: none;
}
.thumb:hover .hv {
	display: inline !important;
	float: right;
}
.thumb {
	border-top-style: solid;
	border-color: black;
	border-width: 1px;
	padding 5px;
	margine 5px;
}
</style>
</head>
<body>
			`)
			for _, x := range files {
				fmt.Fprintf(w, "<div class=\"thumb\"><div class=\"hv\"><img src='/public/%v'></img></div><a href='/public/%v'>%v\n<img style=\"display: block\" src='/public/%v'></img></a></div>\n", sm.EndpointImgNormal(x), sm.EndpointImgOriginal(x), x, sm.EndpointImgThumbnail(x))
			}
			fmt.Fprintln(w, "</body>\n</html>")
		})
		http.Handle("/public/", http.FileServer(http.Dir("./")))
		if err := http.ListenAndServe("localhost:5000", nil); err != nil {
			log.Fatalln(err)
		}
	}()

	for {
		time.Sleep(time.Second)

		ws, err := os.ReadDir("./waiting")
		if err != nil {
			log.Println("read dir", err)
			continue
		}

		for _, x := range ws {
			dat, err := os.ReadFile("./waiting/" + x.Name())
			if err != nil {
				log.Println(err)
				continue
			}

			name, err := sm.ImgStore(bytes.NewReader(dat), strings.SplitN(x.Name(), ".", 2)[1])
			if err != nil {
				log.Println(err)
				continue
			}

			err = os.Remove("./waiting/" + x.Name())
			if err != nil {
				log.Println(err)
				continue
			}

			log.Println(name)
			log.Println(sm.EndpointImgNormal(name))
			log.Println(sm.EndpointImgThumbnail(name))
			log.Println(sm.EndpointImgOriginal(name))
		}
	}
}
