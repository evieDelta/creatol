// only build with the webp tag
// +build !webpencode

package simplemedia

import (
	"image"
	"io"
	"net/http"

	"codeberg.org/eviedelta/creatol/wterr"
	"golang.org/x/image/webp"
)

func webpDecode(datr io.Reader) (image.Image, error) {
	return webp.Decode(datr)
}

func webpEncode(w io.Writer, img image.Image, quality int) error {
	return wterr.New(http.StatusNotImplemented).
		Error("webp encoding is not supported, compile with `-tags webpencode` to encode webp images").
		Msg("webp encoding is not supported").Get()
}
