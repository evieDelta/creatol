//

// only build with the webp tag
// +build webpencode

package simplemedia

import (
	"image"
	"io"

	"github.com/kolesa-team/go-webp/decoder"
	"github.com/kolesa-team/go-webp/encoder"
	"github.com/kolesa-team/go-webp/webp"
)

func webpDecode(datr io.Reader) (image.Image, error) {
	return webp.Decode(datr, &decoder.Options{})
}

func webpEncode(w io.Writer, img image.Image, quality int) error {
	var err error
	var e *encoder.Options
	if quality > 100 {
		e, err = encoder.NewLosslessEncoderOptions(encoder.PresetDefault, quality-100)
	} else {
		e, err = encoder.NewLossyEncoderOptions(encoder.PresetDefault, float32(quality))
	}

	if err != nil {
		return err
	}
	return webp.Encode(w, img, e)
}
