# Creatol for CreatorList
an early development site for listing picrews and other character creators with curated comprehensive tags

## Setup
you'll need to use npm to fetch some 3rdparty assets
and then you'll need to build the static directory, which you can do by navigating a terminal to `web` and running `make`

Building can then be done from cmd/creatorlist with `go build`

if using simplemedia (currently the only and default media file handler) then webp encoding support can be added with the build tag `-tags webpencode` (webp encoding requires the external dependency `libwebp` installed via the OS package manager, so it is not enabled by default)

## Development
For development the following command is used from `cmd/creatorlist/` to run it
```shell
go run -tags webpencode . -media "./public/" -serve-media -templ ../../web/templates -static ../../web/static/
```
Where with `-templ` specified the endpoint `/reltempl` becomes available to be used to reload the templates during runtime

and from `database/psql/`
```
sqlc generate
```
can be used to regenerate the database models

and from `web`
```
make
```
or its various contained actions can be used to regenerate static assets


## LICENSE
   Copyright 2021 DeltaEvie

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.