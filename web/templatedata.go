package web

import (
	"html/template"
	"time"

	"codeberg.org/eviedelta/creatol"
)

type TplGeneralMeta struct {
	Config  PageConfig
	Account AccountInfo
	Nonce   string

	Queries string
}

type AccountInfo struct {
	Session  SessionInfo
	LoggedIn bool

	creatol.UserProfile
}

type SessionInfo struct {
	ID      creatol.SessionID
	Created time.Time
	Expires time.Time
}

func (s *Site) templateFunctions() template.FuncMap {
	return template.FuncMap{
		"uwu": func(a string) string { return "uwuth doth be doth" },
	}
}

// types specific to the base scope are located in push.go

// types specific to error pages

type ErrorContext struct {
	ErrorCode int
	ErrorText string

	ShowRawError bool
	RawError     string
	UserMessage  string
}
