package web

import (
	"embed"
	"encoding/base64"
	"fmt"
	"html/template"
	"io/fs"
	"net/http"
	"os"

	"codeberg.org/eviedelta/creatol/logic"
	"github.com/go-chi/chi/v5"
	"github.com/gorilla/sessions"
)

////go:embed static/*
var static embed.FS

//go:embed templates
var templates embed.FS

type Site struct {
	Opts Options

	Logic *logic.CreatorList
	R     *chi.Mux

	Sess sessions.Store

	templates *template.Template

	TemplateDir string
	StaticDir   string

	TemplFS  fs.FS
	StaticFS fs.FS
}

func (r *Site) Initialise(opts Options) error {
	if opts.PageConfig.MediaDomain == "" {
		opts.PageConfig.MediaDomain = "/media/"
	}

	r.Opts = opts

	// figure out where to serve static items from
	// either the go:embed or a directory if given
	// (useful for developing without recompiling)
	if r.StaticDir == "" {
		sfs, err := fs.Sub(static, "static")
		if err != nil {
			return err
		}
		r.StaticFS = sfs
	} else {
		r.StaticFS = os.DirFS(r.StaticDir)
	}

	// figure out where to load templates from
	// either the go:embed or a directory if given
	// (useful for developing without recompiling)
	if r.TemplateDir == "" {
		sfs, err := fs.Sub(templates, "templates")
		if err != nil {
			return err
		}
		r.TemplFS = sfs
	} else {
		r.TemplFS = os.DirFS(r.TemplateDir)
	}

	{
		var sess *sessions.CookieStore

		sk, err := base64.RawStdEncoding.DecodeString(r.Opts.Sessions.SignKey)
		if err != nil {
			return err
		}

		if r.Opts.Sessions.EncryptionKey != "" {
			ek, err := base64.RawStdEncoding.DecodeString(r.Opts.Sessions.SignKey)
			if err != nil {
				return err
			}

			sess = sessions.NewCookieStore(sk, ek)
		} else {
			sess = sessions.NewCookieStore(sk)
		}

		sess.Options.HttpOnly = true
		sess.Options.Secure = true
		sess.Options.SameSite = http.SameSiteStrictMode

		r.Sess = sess
	}

	// load templates
	err := r.loadTemplates()
	if err != nil {
		return err
	}

	return r.SetupChi()
}

func (r *Site) Run(url string) error {
	return http.ListenAndServe(url, r.R)
}

func (r *Site) loadTemplates() error {
	t, err := template.New("root").Funcs(r.templateFunctions()).ParseFS(r.TemplFS, "*.html", "*/*.html")
	if err != nil {
		return err
	}
	r.templates = t

	return nil
}

func (s *Site) reloadTemplates(rw http.ResponseWriter, r *http.Request) {
	if s.TemplateDir == "" {
		rw.WriteHeader(http.StatusTeapot) // this should never appear
		return
	}

	rw.Header().Add("content-type", "text/html")

	err := s.loadTemplates()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "<code>%v</code>\n", err)
	} else {
		rw.WriteHeader(http.StatusOK)
	}

	fmt.Fprintf(rw, `<form><label>Reload Templates</label><input type='submit'/></form>`)
}
