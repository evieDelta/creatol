package web

import (
	"net/http"
)

// Homepage .
func (s *Site) Homepage(w http.ResponseWriter, r *http.Request) {
	s.WrapRun(w, r, "Homepage", func() error {
		return s.ShowPage(w, r, PageParameters{
			Head: TitleHead("CreatorList"),
		})
	})
}

/*
func init() {
	debug.SetMaxStack(1 * 1000 * 1000 * 100)

	go func() {
		t := time.NewTicker(time.Second)

		var pv uint64
		var uhoh int
		for {
			<-t.C

			sample := make([]metrics.Sample, 1)
			sample[0].Name = "/memory/classes/total:bytes"

			metrics.Read(sample)

			if sample[0].Value.Kind() == metrics.KindBad {
				panic("metric no longer supported")
			}

			val := sample[0].Value.Uint64() / 1000

			fmt.Print(val, "k ")

			if val-pv > 10000 {
				uhoh++

				if uhoh > 3 {
					log.Fatalln("\nRunaway memory usage detected, halted for safety", val, "k")
				}
			} else {
				uhoh = 0
			}

			pv = val
		}
	}()
}
*/
