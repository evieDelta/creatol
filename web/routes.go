package web

import (
	"bytes"
	"context"
	crand "crypto/rand"
	"encoding/json"
	"fmt"
	"html"
	"html/template"
	"io"
	"io/fs"
	"log"
	"math"
	"math/big"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
	"unicode"

	"codeberg.org/eviedelta/creatol"
	"codeberg.org/eviedelta/creatol/wterr"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func (s *Site) HeaderMiddleware(h http.Handler) http.Handler {
	type Pair struct{ key, value string }

	keys := []Pair{
		{"X-Content-Type-Options", "nosniff"},
		{"X-Frame-Options", "DENY"},
		{"X-XSS-Protection", "1; mode=block"},
		{"Referrer-Policy", "same-origin"},
	}

	fn := func(w http.ResponseWriter, r *http.Request) {
		for _, x := range keys {
			w.Header().Set(x.key, x.value)
		}

		h.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func (s *Site) CSPMiddleware(h http.Handler) http.Handler {
	brin, err := crand.Int(crand.Reader, big.NewInt(math.MaxInt64))
	if err != nil {
		panic(err)
	}
	rin := brin.Int64()

	fn := func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("csp a", r.RequestURI)

		// not entirely the most cryptographically bullet proof random source
		// ideally it would just generate a new crypto/rand number each time
		// but idk how much random entropy there is so i'd rather not hog it
		// and i mean, its not like a picrew curator is going to be a
		// big enough target for this to matter anyways, if somebody can
		// brute force a random number, a psudo random number with random seed
		// (set and periodically reset in /cmd/creatorlist/main.go#init) and be
		// precise enough to guess a psudo random number with a nanosecond seed
		// and bypass the code injection protection offered by html/template
		// as well as the complete script denial of icons in the media server
		// then they've probably got bigger targets to fry
		nonce := rin ^ rand.Int63() ^ rand.New(rand.NewSource(time.Now().UnixNano())).Int63()
		r = r.WithContext(context.WithValue(r.Context(), ContextKeyNonce, nonce))

		w.Header().Set("Content-Security-Policy", fmt.Sprintf(""+
			"base-uri 'self'; default-src 'self'; img-src 'self' %v; font-src 'self';"+
			"script-src 'strict-dynamic' 'nonce-%v'; style-src 'self' https://unpkg.com; object-src 'none';"+
			"report-uri /csp-notify",
			s.Opts.PageConfig.MediaDomain, strconv.FormatInt(nonce, 36),
		))

		h.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func (r *Site) SetupChi() error {
	r.R = chi.NewMux()

	e, err := fs.ReadDir(r.StaticFS, ".")
	if err != nil {
		return err
	}
	for _, x := range e {
		fmt.Println(x.Name(), x.Type().String())
	}

	r.R.Use(
		middleware.Heartbeat("/ping"),
		middleware.Logger,
		middleware.RequestID,
		middleware.Recoverer,
		func(h http.Handler) http.Handler {
			fn := func(w http.ResponseWriter, r *http.Request) {
				r = r.WithContext(context.WithValue(r.Context(), creatol.QueryStatKey, new(creatol.QueryStat)))
				h.ServeHTTP(w, r)
			}
			return http.HandlerFunc(fn)
		},
		r.HeaderMiddleware,
		r.CSPMiddleware,
	)

	r.R.Group(r.RoutesBrowse)
	r.R.Route("/account", r.RoutesAccount)

	if r.TemplateDir != "" {
		r.R.Get("/reltempl", r.reloadTemplates)
	}

	r.R.NotFound(r.theMostHorrible404)

	r.R.Handle("/static/*", http.StripPrefix("/static/", http.FileServer(http.FS(r.StaticFS))))

	r.R.Post("/csp-notify", func(rw http.ResponseWriter, r *http.Request) {
		dat, err := io.ReadAll(r.Body)
		if err != nil {
			log.Println("csp notify", err)
			return
		}

		b := &bytes.Buffer{}
		err = json.Indent(b, dat, "", "\t")
		if err != nil {
			log.Println("csp notify", err)
		}

		fmt.Printf("csp alert:\n\t%v\n", string(b.Bytes()))

		rw.WriteHeader(http.StatusOK)
	})

	r.R.HandleFunc("/icons/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Location", strings.Replace(strings.ToLower(r.RequestURI), "/icons/", "/icon/", 1))
		w.WriteHeader(http.StatusPermanentRedirect)
	})
	r.R.Route("/icon", r.RoutesIcon)

	return nil
}

func (s *Site) RoutesBrowse(r chi.Router) {
	r.Use(
		middleware.Heartbeat("/ping"),
		s.CheckAuthentication,
	)

	r.Get("/", s.Homepage)
	r.Get("/index", s.Homepage)

}

func (s *Site) RoutesAccount(r chi.Router) {
	r.Use(
		middleware.Heartbeat("/ping"),
		//	s.HeaderMiddleware,
		//	s.CSPMiddleware,
		s.CheckAuthentication,
	)

	// public login/registration
	r.Group(func(r chi.Router) {
		r.Get("/register", s.GetRegister)
		r.Get("/register/namepass", s.GetRegisterNamepass)
		r.With(middleware.AllowContentType("application/x-www-form-urlencoded")).
			Post("/register/namepass", s.PostRegisterNamepass)
		r.Get("/login", s.GetLogin)
		r.Get("/login/namepass", s.GetLoginNamepass)
		r.With(middleware.AllowContentType("application/x-www-form-urlencoded")).
			Post("/login/namepass", s.PostLoginNamepass)

		r.Get("/logout", s.GetLogout)
	})

	// account endpoints that require being logged in
	r.Group(func(r chi.Router) {
		r.Use(s.MiddlewareRequireAuth)

		r.HandleFunc("/*", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Location", "/account/settings")
			w.WriteHeader(http.StatusTemporaryRedirect)
		})
	})
}

func (s *Site) WrapRun(w http.ResponseWriter, r *http.Request, n string, fn func() error) {
	err := fn()
	if err == nil {
		return
	}
	log.Println(n+":", err)

	if e, ok := err.(wterr.Error); ok {
		s.HandleWterr(w, r, e)
	} else {
		s.HandleGenericError(w, r, err)
	}
}

// i'm sorry
func (s *Site) theMostHorrible404(w http.ResponseWriter, r *http.Request) {
	if strings.IndexFunc(r.RequestURI, unicode.IsUpper) != -1 {
		w.Header().Set("Location", strings.ToLower(r.RequestURI))
		w.WriteHeader(http.StatusPermanentRedirect)
		return
	}

	w.Header().Set("Content-Security-Policy", "any 'self'")

	w.WriteHeader(404)
	ct := fmt.Sprintf(`
		<div style="background: linear-gradient(to bottom right, #E673A7 0%%, #994c5f 100%%); height: 100%%">
		<head><link rel="stylesheet" href="/static/external/normalize.css"></head>
		<style>#xbutton:hover { background-color: #994C5F !important; box-shadow: 2px 2px 4px #FF99BA !important; }</style>
		<div style="max-width: 30em; margin: auto; margin-top: 5em; color: #383235; box-shadow: 20px 20px 30px #804060;">
		<div style="display: flex; clear: both; padding: 0em 1.25em; background-color: #FFABD2; width: max; font-size: 95%%; text-shadow: 2px 2px 4px #FFCCDC;">
		<h3 style="margin: 0em; padding: 0.15em 0em 0.12em; float: left">404 not found</h3>
		<a id="xbutton" style="font-family: monospace; margin: 0em 0em 0em auto; padding: 0.25em 0.33em; background-color: #D96C87; text-decoration: none; display: grid" href="/"><img src="/static/assets/svg/roundedXwhite.svg" style="width: 1.5em; height: 1.5em; margin: auto"/></a>
		</div>
		<div style="padding: 0.5em 1.25em 1.75em 1em; margin: 0em; background-color: #E6E1E3; color: #383235">
		<p style="margin: 0em 0em 0.75em">I'm sorry we could not find what you were looking for</p>
		<code style="border-left: 0.25em solid #804060; background-color: #CCCACB; padding: 0.25em 0.33em">%v : %v</code></div></div></div>
		`,
		r.Method, html.EscapeString(r.URL.String()))

	err := s.ShowPage(w, r, PageParameters{
		Content: template.HTML(ct),
		Head:    TitleHead("404 Not Found"),
	})
	if err != nil {
		log.Println("!", err)
		return
	}
}

func (s *Site) HandleWterr(w http.ResponseWriter, r *http.Request, e wterr.Error) {
	log.Println(e.Type, e.Err, e.Str)

	var templ string
	switch e.Type {

	default:
		templ = "error_generic"
	}

	w.WriteHeader(e.StatusCode())

	var raw string
	if s.Opts.RevealRawErr && e.Err != nil {
		raw = e.ErrStr()
	}

	buf := &strings.Builder{}
	err := s.templates.ExecuteTemplate(buf, templ, ErrorContext{
		ErrorCode: e.Type,
		ErrorText: e.StatusCodeStr(),

		ShowRawError: s.Opts.RevealRawErr,
		UserMessage:  e.MsgStr(),
		RawError:     raw,
	})
	if err != nil {
		log.Println("!", err)
		return
	}

	err = s.ShowPage(w, r, PageParameters{
		Content: template.HTML(buf.String()),
		Head:    TitleHead("Error " + e.StatusCodeStr()),
	})
	if err != nil {
		log.Println("!", err)
		return
	}
}

func (s *Site) HandleGenericError(w http.ResponseWriter, r *http.Request, e error) {
	w.WriteHeader(http.StatusInternalServerError)

	log.Println(e)

	buf := &strings.Builder{}
	err := s.templates.ExecuteTemplate(buf, "error_generic", ErrorContext{
		ErrorCode: 500,

		ShowRawError: s.Opts.RevealRawErr,
		RawError:     e.Error(),
	})
	if err != nil {
		log.Println("!", err)
		return
	}

	err = s.ShowPage(w, r, PageParameters{
		Content: template.HTML(buf.String()),
		Head:    TitleHead("Error 500"),
	})
	if err != nil {
		log.Println("!", err)
		return
	}
}
