package web

import (
	"html/template"
	"net/http"
	"strconv"

	"codeberg.org/eviedelta/creatol"
)

// GetGeneralMeta fetches some general purpose metadata for templates
func (s *Site) GetGeneralMeta(rw http.ResponseWriter, r *http.Request) (t TplGeneralMeta, err error) {
	a, ok := ContextGetAuth(r.Context())
	if ok {
		t.Account = a
	}

	t.Config = s.Opts.PageConfig
	n, _ := ContextGetNonce(r.Context())
	t.Nonce = strconv.FormatInt(n, 36)

	t.Queries = creatol.GetQueryStat(r.Context()).String()

	return t, nil
}

type pushTempl struct {
	PageParameters

	TplGeneralMeta
}

type PageParameters struct {
	RequestBase string
	RequestFull string

	Content, Sidebar template.HTML
	Head             Head
}

func (s *Site) ShowPage(w http.ResponseWriter, r *http.Request, p PageParameters) error {
	if p.Content == "" {
		p.Content = "<code>No content.</code>"
	}

	t, err := s.GetGeneralMeta(w, r)
	if err != nil {
		return err
	}

	return s.templates.ExecuteTemplate(w, "base", pushTempl{
		PageParameters: p,
		TplGeneralMeta: t,
	})
}

type Head struct {
	Title string

	Extras []template.HTML
}

func TitleHead(t string) Head { return Head{Title: t} }
