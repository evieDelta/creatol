package web

type Options struct {
	RevealRawErr bool

	PageConfig PageConfig

	Sessions struct {
		SignKey       string
		EncryptionKey string // optional, must be 16 24 or 32 bytes
	}
}

type PageConfig struct {
	UseCDN struct {
		CSS bool
	}
	MediaDomain string
}
