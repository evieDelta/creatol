package web

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func (s *Site) IconCSPMiddleware(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Security-Policy", "default-src 'none'; image-src 'self'; report-uri /csp-notify") // yeah no svg injection for you
		//	w.Header().Set("Content-Security-Policy", "default-src 'self';"+
		//		" child-src 'none'; connect-src 'none'; frame-src 'none'; manifest-src 'none';"+
		//		" media-src 'none'; object-src 'none'; script-src 'none'; worker-src 'none';") // bitch

		h.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func (s *Site) RoutesIcon(r chi.Router) {
	r.Use(
		middleware.CleanPath,
		middleware.Compress(5, "image/svg+xml"),
		s.IconCSPMiddleware,
		middleware.SetHeader("Cache-Control", "public max-age=31536000"), // we don't expect them to change, so lets just set it indefinitely
	)

	r.NotFound(func(wr http.ResponseWriter, r *http.Request) {
		fmt.Fprint(wr, "404 not found.")
		wr.Header().Set("content-type", "text/plain")
		wr.WriteHeader(404)
	})
	r.Get("/svg/{id}/{name}", s.IconSVG)
}

func (s *Site) IconSVG(wr http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	name := chi.URLParam(r, "name")

	fmt.Println(id, name)

	wr.Header().Set("content-type", "image/svg+xml")

	// wr.Header().Set("content-type", "text/html")
	/*	fmt.Fprint(wr, `<?xml version="1.0" standalone="no"?>
		<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

		<svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg">
		<polygon id="triangle" points="0,0 0,50 50,0" fill="#009900" stroke="#004400"/>
		<script type="text/javascript">
		alert('This app is probably vulnerable to XSS attacks!');
		</script>
		</svg>`)*/

	fmt.Fprint(wr, `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>`)
}
