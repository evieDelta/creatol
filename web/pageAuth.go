package web

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"

	"codeberg.org/eviedelta/creatol"
	"codeberg.org/eviedelta/creatol/constants"
	"codeberg.org/eviedelta/creatol/wterr"
)

type templEnabledLoginMethods struct {
	UsernamePassword bool
	EmailPassword    bool
	DiscordOAuth2    bool
}

type templGetRegister struct {
	EnabledMethods templEnabledLoginMethods
}

func (s *Site) GetRegister(w http.ResponseWriter, r *http.Request) {
	s.WrapRun(w, r, "Register", func() error {
		b := &bytes.Buffer{}

		err := s.templates.ExecuteTemplate(b, "auth-register", templGetRegister{
			EnabledMethods: templEnabledLoginMethods{
				UsernamePassword: s.Logic.Auth.Config.EnabledMethods.UsernamePassword,
			},
		})
		if err != nil {
			return err
		}

		return s.ShowPage(w, r, PageParameters{
			Head:    TitleHead("Register"),
			Content: template.HTML(b.String()),
		})
	})
}

func (s *Site) GetRegisterNamepass(w http.ResponseWriter, r *http.Request) {
	s.WrapRun(w, r, "Register Name+Pass", func() error {
		if !s.Logic.Auth.Config.EnabledMethods.UsernamePassword {
			w.Header().Set("Location", "/account/register")
			w.WriteHeader(http.StatusTemporaryRedirect)
			return nil
		}

		b := &bytes.Buffer{}

		err := s.templates.ExecuteTemplate(b, "auth-register-namepass", nil)
		if err != nil {
			return err
		}

		return s.ShowPage(w, r, PageParameters{
			Head:    TitleHead("Register"),
			Content: template.HTML(b.String()),
		})
	})
}

func (s *Site) PostRegisterNamepass(w http.ResponseWriter, r *http.Request) {
	s.WrapRun(w, r, "Register Name+Pass Post", func() error {
		if r.Header.Get("Content-Type") != "application/x-www-form-urlencoded" {
			return wterr.New(http.StatusUnsupportedMediaType).Get()
		}

		if !s.Logic.Auth.Config.EnabledMethods.UsernamePassword {
			w.Header().Set("Location", "/account/register")
			w.WriteHeader(http.StatusSeeOther)
			return nil
		}

		dt, err := io.ReadAll(r.Body)

		log.Println(string(dt))

		q, err := url.ParseQuery(string(dt))
		if err != nil {
			return wterr.New(400).Msg(err).Get()
		}

		logn := strings.ToLower(q.Get("username"))
		pass := q.Get("password")
		dsnm := q.Get("display-name")
		invt := q.Get("invite-code")

		dsnm, ok := creatol.SanitizeDisplayname(dsnm)
		if !ok {
			return wterr.New(http.StatusNotAcceptable).Msg("invalid display name").Get()
		}

		if !s.Logic.Auth.CanRegister(constants.AuthMethodUsernamePassword, invt) {
			return wterr.New(http.StatusNotAcceptable).Msg("unable to register (probably an invalid invite code)").Get()
		}

		tok, err := s.Sess.New(r, SessionKeyAuthentication)
		if err != nil {
			return err
		}

		prof, sess, err := s.Logic.Auth.NamePassRegister(r.Context(), r.RemoteAddr, r.Header.Get("User-Agent"), dsnm, logn, pass)
		if err != nil {
			return err
		}

		dat, err := json.Marshal(sess)
		if err != nil {
			return err
		}

		tok.Values[SessionKeyValueAuthenticationLogin] = string(dat)
		err = tok.Save(r, w)
		if err != nil {
			return err
		}

		acc := AccountInfo{
			LoggedIn:    true,
			UserProfile: prof,
			Session: SessionInfo{
				ID:      sess.ID,
				Created: sess.Created,
				Expires: sess.Expires,
			},
		}

		r = r.WithContext(context.WithValue(r.Context(), ContextKeyAuth, acc))

		return s.ShowPage(w, r, PageParameters{
			Head:    TitleHead("Register"),
			Content: template.HTML(fmt.Sprintf("<code>%+v</code>\n<code>%+v</code>", prof, sess)),
		})
	})
}

func (s *Site) GetLogin(w http.ResponseWriter, r *http.Request) {
	s.WrapRun(w, r, "Register", func() error {
		b := &bytes.Buffer{}

		err := s.templates.ExecuteTemplate(b, "auth-login", templGetRegister{
			EnabledMethods: templEnabledLoginMethods{
				UsernamePassword: s.Logic.Auth.Config.EnabledMethods.UsernamePassword,
			},
		})
		if err != nil {
			return err
		}

		return s.ShowPage(w, r, PageParameters{
			Head:    TitleHead("Login"),
			Content: template.HTML(b.String()),
		})
	})

	//	s.WrapRun(w, r, "Login", func() error {
	//		a, _ := ContextGetAuth(r.Context())
	//
	//		return s.ShowPage(w, r, PageParameters{
	//			Head: TitleHead("Login"),
	//
	//			Content: template.HTML(fmt.Sprintf("<code>Li? %v, UID: %v, UN: %v\n</code>", a.LoggedIn, a.Name)),
	//		})
	//	})
}

func (s *Site) GetLoginNamepass(w http.ResponseWriter, r *http.Request) {
	s.WrapRun(w, r, "Login Name+Pass", func() error {
		b := &bytes.Buffer{}

		err := s.templates.ExecuteTemplate(b, "auth-login-namepass", nil)
		if err != nil {
			return err
		}

		return s.ShowPage(w, r, PageParameters{
			Head:    TitleHead("Login"),
			Content: template.HTML(b.String()),
		})
	})
}

func (s *Site) PostLoginNamepass(w http.ResponseWriter, r *http.Request) {
	s.WrapRun(w, r, "Register Name+Pass Post", func() error {
		if r.Header.Get("Content-Type") != "application/x-www-form-urlencoded" {
			return wterr.New(http.StatusUnsupportedMediaType).Get()
		}

		dt, err := io.ReadAll(r.Body)

		log.Println(string(dt))

		q, err := url.ParseQuery(string(dt))
		if err != nil {
			return wterr.New(400).Msg(err).Get()
		}

		logn := strings.ToLower(q.Get("username"))
		pass := q.Get("password")

		tok, err := s.Sess.New(r, SessionKeyAuthentication)
		if err != nil {
			return err
		}

		prof, sess, err := s.Logic.Auth.NamePassLogin(r.Context(), r.RemoteAddr, r.Header.Get("User-Agent"), logn, pass)
		if err != nil {
			return err
		}

		dat, err := json.Marshal(sess)
		if err != nil {
			return err
		}

		tok.Values[SessionKeyValueAuthenticationLogin] = string(dat)
		err = tok.Save(r, w)
		if err != nil {
			return err
		}

		acc := AccountInfo{
			LoggedIn:    true,
			UserProfile: prof,
			Session: SessionInfo{
				ID:      sess.ID,
				Created: sess.Created,
				Expires: sess.Expires,
			},
		}

		r = r.WithContext(context.WithValue(r.Context(), ContextKeyAuth, acc))

		return s.ShowPage(w, r, PageParameters{
			Head:    TitleHead("Register"),
			Content: template.HTML(fmt.Sprintf("<code>%+v</code>\n<code>%+v</code>", prof, sess)),
		})
	})
}

func (s *Site) GetLogout(w http.ResponseWriter, r *http.Request) {
	s.WrapRun(w, r, "Login Name+Pass", func() error {
		ao, ok := ContextGetAuth(r.Context())
		if !ok {
			return s.ShowPage(w, r, PageParameters{
				Head:    TitleHead("logout"),
				Content: template.HTML("Logged out. <a href='/'>Home</a>"),
			})
		}

		err := s.Logic.Auth.InvalidateSessionID(r.Context(), ao.Session.ID)
		if err != nil {
			return err
		}

		tok, err := s.Sess.Get(r, SessionKeyAuthentication)
		if err != nil {
			return err
		}

		err = s.ClearAuthentication(w, r, tok)
		if err != nil {
			return err
		}

		return s.ShowPage(w, r, PageParameters{
			Head:    TitleHead("logout"),
			Content: template.HTML("Logged out. <a href='/'>Home</a>"),
		})
	})
}
