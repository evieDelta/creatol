package web

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"net/url"

	"codeberg.org/eviedelta/creatol/auth"
	"emperror.dev/errors"
	"github.com/gorilla/sessions"
)

type ContextKey string

const ContextKeyAuth ContextKey = "authentication"
const ContextKeyNonce ContextKey = "nonce"

const SessionKeyAuthentication = "authentication"
const SessionKeyValueAuthenticationLogin = "login"

func ContextGetAuth(ctx context.Context) (AccountInfo, bool) {
	v := ctx.Value(ContextKeyAuth)
	if v == nil {
		return AccountInfo{}, false
	}

	a, ok := v.(AccountInfo)
	return a, ok
}

func ContextGetNonce(ctx context.Context) (int64, bool) {
	v := ctx.Value(ContextKeyNonce)
	if v == nil {
		return 0, false
	}

	a, ok := v.(int64)
	return a, ok
}

func (s *Site) ClearAuthentication(rw http.ResponseWriter, r *http.Request, tok *sessions.Session) error {
	tok.Options.MaxAge = -1
	err := tok.Save(r, rw)
	if err != nil {
		return err
	}

	return nil
}

func (s *Site) CheckAuthentication(h http.Handler) http.Handler {
	fn := func(rw http.ResponseWriter, r *http.Request) {
		if _, ok := ContextGetAuth(r.Context()); ok {
			h.ServeHTTP(rw, r)
			return
		}

		s.WrapRun(rw, r, "check-auth", func() error {
			tok, err := s.Sess.Get(r, SessionKeyAuthentication)
			if err != nil {
				log.Println("auth g ses:", err)
				return s.ClearAuthentication(rw, r, tok)
			}
			if !tok.IsNew {
				d, ok := tok.Values[SessionKeyValueAuthenticationLogin].(string)
				if !ok {
					log.Println("auth g v ok: ", ok)
					return s.ClearAuthentication(rw, r, tok)
				}

				var ses auth.ClientSession
				err = json.Unmarshal([]byte(d), &ses)
				if err != nil {
					log.Println("auth u v err: ", err)
					return errors.Append(err, s.ClearAuthentication(rw, r, tok))
				}

				ok, err := s.Logic.Auth.ValidateSession(r.Context(), ses)
				if err != nil {
					log.Println("auth u v avs err: ", err)
					return errors.Append(err, s.ClearAuthentication(rw, r, tok))
				}

				if !ok {
					log.Println("auth g v ok: ", ok)
					return s.ClearAuthentication(rw, r, tok)
				}

				prof, err := s.Logic.DB.UserProfileGetByID(r.Context(), ses.UserID)
				if err != nil {
					log.Println("auth upg err:", err)
					return err
				}

				acc := AccountInfo{
					LoggedIn:    true,
					UserProfile: prof,
					Session: SessionInfo{
						ID:      ses.ID,
						Created: ses.Created,
						Expires: ses.Expires,
					},
				}

				r = r.WithContext(context.WithValue(r.Context(), ContextKeyAuth, acc))
			}
			return nil
		})

		h.ServeHTTP(rw, r)
	}

	return http.HandlerFunc(fn)
}

func (s *Site) MiddlewareRequireAuth(h http.Handler) http.Handler {
	fn := func(rw http.ResponseWriter, r *http.Request) {
		a, ok := ContextGetAuth(r.Context())
		if !ok || !a.LoggedIn {
			rw.Header().Set("Location", "/account/login?redir="+r.URL.EscapedPath()+url.QueryEscape("?"+r.URL.RawQuery))
			rw.WriteHeader(http.StatusFound)
			return
		}

		h.ServeHTTP(rw, r)
	}
	return http.HandlerFunc(fn)
}
