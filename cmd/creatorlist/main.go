package main

import (
	crand "crypto/rand"
	"encoding/base64"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"math"
	"math/big"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"codeberg.org/eviedelta/creatol/database/psql"
	"codeberg.org/eviedelta/creatol/logic"
	"codeberg.org/eviedelta/creatol/simplemedia"
	"codeberg.org/eviedelta/creatol/web"
	"emperror.dev/errors"
	"github.com/pelletier/go-toml/v2"
)

func Initialise(configdir string) (*logic.CreatorList, web.Options, error) {
	c := &logic.CreatorList{}
	wopts := web.Options{}

	d := os.DirFS(configdir)

	bc, err := d.Open("config.toml")
	if err != nil {
		return nil, wopts, err
	}
	dec := toml.NewDecoder(bc)
	dec.Decode(&c.Config)
	bc.Close()

	wc, err := d.Open("web.toml")
	if err != nil {
		return nil, wopts, err
	}
	dec = toml.NewDecoder(wc)
	dec.Decode(&wopts)
	wc.Close()

	switch strings.ToLower(c.Config.MediaHandler) {
	default:
		fallthrough
	case "simple":
		c.Media = simplemedia.New()
	}

	mc, err := fs.ReadFile(d, "media.toml")
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			err := os.WriteFile(filepath.Join(configdir, "media.toml"), []byte(c.Media.GetDefaultConfig()), 0664)
			if err != nil {
				return nil, wopts, err
			}
			return nil, wopts, errors.NewPlain("written media handler config")
		}
		return nil, wopts, err
	}

	err = c.Media.Initialise(string(mc))
	if err != nil {
		return nil, wopts, err
	}

	switch strings.ToLower(c.Config.Database.Type) {
	default:
		fallthrough
	case "psql", "postgres":
		c.DB = &psql.Database{}
	}

	return c, wopts, nil
}

func main() {
	var templd, staticd, mediad, configd, url string
	var servmedia, genkey bool
	flag.StringVar(&templd, "templ", "", "override the page generation templates, don't use this in production as it enables the unprotected /reltempl endpoint")
	flag.StringVar(&staticd, "static", "", "override the static resources dir")
	flag.StringVar(&configd, "config", "./config/", "the directory to load config files from")
	flag.StringVar(&mediad, "media", "./media/", "the media directory (only used if serve-media is enabled)")
	flag.BoolVar(&servmedia, "serve-media", false, "serve at /media/ the contents of the media directory")
	flag.BoolVar(&genkey, "gen-key", false, "generate a secure random key for use as a session signing or encryption key")
	flag.StringVar(&url, "url", ":7380", "the url and port to run off of")
	flag.Parse()

	if genkey {
		var b = make([]byte, 32)
		_, err := rand.Read(b)
		if err != nil {
			panic(err)
		}
		s := base64.RawStdEncoding.EncodeToString(b)
		fmt.Println(s)

		return
	}

	cl, wc, err := Initialise(configd)
	if err != nil {
		log.Fatalln(err)
		return
	}

	err = cl.Initialise()
	if err != nil {
		log.Fatalln(err)
		return
	}

	s := web.Site{
		TemplateDir: templd,
		StaticDir:   staticd,

		Logic: cl,
	}

	err = s.Initialise(wc)
	if err != nil {
		log.Fatalln(err)
		return
	}
	log.Println("Running on", url)
	if servmedia {
		s.R.Handle("/media/", http.FileServer(http.Dir(mediad)))
	}
	err = http.ListenAndServe(url, s.R)
	log.Println(err)
}

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	go func() {
		for {
			time.Sleep(time.Minute * 10)

			// make the output of the psudo random a little more unpredictable
			// without access to the program state
			brin, err := crand.Int(crand.Reader, big.NewInt(math.MaxInt64))
			if err != nil {
				panic(err)
			}
			rin := brin.Int64()

			rand.Seed(rin)
		}
	}()
}
