package logic

import (
	"context"

	"codeberg.org/eviedelta/creatol"
	"codeberg.org/eviedelta/creatol/auth"
)

type Config struct {
	Database struct {
		Type       string // postgresql
		ConnString string
	}
	Auth         auth.AuthConfig
	MediaHandler string // simple
}

func New() *CreatorList {
	return &CreatorList{}
}

type CreatorList struct {
	DB    creatol.Database
	Media creatol.MediaFiles
	Auth  *auth.Auth

	Config Config
}

func (c *CreatorList) Initialise() error {
	err := c.DB.Open(context.TODO(), c.Config.Database.ConnString)
	if err != nil {
		return err
	}
	c.Auth = &auth.Auth{DB: c.DB.(creatol.AuthDatabase), Config: c.Config.Auth, Media: c.Media}

	return nil
}

func (c *CreatorList) Close() error {
	return c.DB.Close(context.TODO())
}
