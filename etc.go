package creatol

import (
	"strings"
	"unicode"
	"unicode/utf8"
)

const (
	DisplaynameMinLen    = 3
	DisplaynameMaxLen    = 32
	DisplaynameMaxMarks  = 3
	DisplaynameMaxSpaces = 2
)

// SanitizeDisplayname returns a cleaned up display name
// removing any control characters or excessive diacritics
// if a name is invalid (too short or too long) it'll return false
func SanitizeDisplayname(s string) (string, bool) {
	// lets just not bother with invalid/broken strings
	if !utf8.ValidString(s) {
		return "", false
	}

	// purge any control or non-print characters, it'll break RTL text
	// but we don't support languages other than english anyways
	// will also prune any whitespace other than regular spaces
	n := strings.Map(func(r rune) rune {
		if !unicode.IsPrint(r) {
			return -1
		}
		return r
	}, s)

	// trim excessive combining marks/diacritics and stuff
	ct := 0
	n = strings.Map(func(r rune) rune {
		if unicode.IsMark(r) {
			ct++
		} else {
			ct = 0
		}

		if ct > DisplaynameMaxMarks {
			return -1
		}
		return r
	}, n)

	// trim excessive spaces
	ct = 0
	n = strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			ct++
		} else {
			ct = 0
		}

		if ct > DisplaynameMaxSpaces {
			return -1
		}
		return r
	}, n)

	// if its too short or too long don't accept it
	if l := len([]rune(n)); l < DisplaynameMinLen || l > DisplaynameMaxLen {
		return "", false
	}

	// if it consists entirely of spaces don't accept it
	// ( it should be impossible to get here
	//   as the excess space trimmer will bring it under a length of 3
	//   but just in case )
	if strings.IndexFunc(n, func(r rune) bool { return !unicode.IsSpace(r) }) == -1 {
		return "", false
	}

	return n, true
}
