package creatol

import (
	"crypto/sha256"
	"time"

	"codeberg.org/eviedelta/creatol/constants"
)

func HashText(s string) string {
	b := sha256.Sum256([]byte(s))
	return string(b[:])
}

// WikiPage is a page of a wiki
type WikiPage struct {
	Address  string
	Date     time.Time
	Revision string

	Markup constants.MarkupType
	Text   string
}

type WikiPageRevision struct {
	Address  string
	Date     time.Time
	Revision string

	Markup constants.MarkupType
	Text   string
}
