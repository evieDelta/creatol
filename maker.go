package creatol

import "codeberg.org/eviedelta/creatol/constants"

// Maker is an entry in the list
type Maker struct {
	ID   MakerID
	Type constants.MakerType

	// these values may be auto filled out depending on the maker used
	Name        string
	Thumbnail   string // the name of the image file used for a thumbnail (hosted locally)
	Description string // description of a maker, best if its a translation of the original

	Tags []TagAssignment

	Meta map[string]string
}
