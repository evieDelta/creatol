package creatol

import "codeberg.org/eviedelta/creatol/constants"

// TagAssignment is an assignment of a tag
type TagAssignment struct {
	TagID   TagID
	MakerID MakerID
	Rating  constants.TagRating
}

// Tag contains the core meta of a Tag
type Tag struct {
	ID          TagID
	Name        string   // the primary name of the tag
	Aliases     []string // other names it can go by
	Description string   // a short description of the tag
	//                   // extended info can go in the wiki pages located at /tags/<id>

	Icon IconRef

	// Optionally specify a single tag which this tag is directly related to
	ParentID TagID
	// Full name including the name of parent tags (separated by slashes)
	FullName string

	// The maker types this tag is allowed to apply to
	// if nil it'll be allowed to apply to all
	AllowedMakerTypes []constants.MakerType
	// The maker types this tag is not allowed to apply to
	// this cannot be used with AllowedMakerTypes
	DisallowedMakerTypes []constants.MakerType
}
