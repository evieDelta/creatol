package psql

import (
	"context"

	"codeberg.org/eviedelta/creatol"
	"codeberg.org/eviedelta/creatol/database/psql/internal/models"
)

func (d *Database) UserFullProfileGetByID(ctx context.Context, uid creatol.UserID) (creatol.UserProfileMeta, error) {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	p, err := d.m.UserProfileGetFullByID(ctx, int64(uid))
	if err != nil {
		return creatol.UserProfileMeta{}, err
	}

	return creatol.UserProfileMeta{
		UserProfile: creatol.UserProfile{
			ID:     creatol.UserID(p.ID),
			Name:   p.Name,
			Avatar: p.Avatar.String,
			Joined: p.Joined,
		},
		Managed:      p.Managed,
		AvatarSource: p.AvatarSource.String,
	}, nil
}

func (d *Database) UserProfileGetByID(ctx context.Context, uid creatol.UserID) (creatol.UserProfile, error) {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	p, err := d.m.UserProfileGetByID(ctx, int64(uid))
	if err != nil {
		return creatol.UserProfile{}, err
	}

	return creatol.UserProfile{
		ID:     creatol.UserID(p.ID),
		Name:   p.Name,
		Avatar: p.Avatar.String,
		Joined: p.Joined,
	}, nil
}

func (d *Database) UserProfileUpdate(ctx context.Context, new creatol.UserProfileMeta) error {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	tx, err := d.Conn.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)
	t := d.m.WithTx(tx)

	err = t.UserProfileUpdate(ctx, models.UserProfileUpdateParams{
		Avatar:       nullString(new.Avatar),
		Name:         new.Name,
		AvatarSource: nullString(new.AvatarSource),
		Managed:      new.Managed,
		ID:           int64(new.ID),
	})
	if err != nil {
		return err
	}

	return tx.Commit(ctx)
}
