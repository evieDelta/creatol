
-- +migrate Up
CREATE TABLE public.users_auth
(
    id bigint NOT NULL,
    source text,
    login_method smallint NOT NULL,
    passkey bytea,
    meta bytea,
    CONSTRAINT users_auth_pkey PRIMARY KEY (id),
    CONSTRAINT users_auth_source_method UNIQUE (source, login_method)
);

COMMENT ON CONSTRAINT users_auth_source_method ON public.users_auth
    IS 'user auth must have a unique combination of source and method';

CREATE INDEX users_auth_source
    ON public.users_auth USING btree
    (source ASC NULLS LAST)
    INCLUDE(id, login_method, passkey);

CREATE TABLE public.users_session
(
    session_id bigint NOT NULL,
    user_id bigint NOT NULL,
    key bytea NOT NULL,
    invalid_after timestamp without time zone NOT NULL,
    last_used timestamp without time zone NOT NULL,
    ip_address text NOT NULL,
    user_agent text NOT NULL,
    PRIMARY KEY (session_id),
    CONSTRAINT fk_user_session_id FOREIGN KEY (user_id)
        REFERENCES public.users_auth (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);

CREATE TABLE public.users_profile
(
    id bigint NOT NULL,
    name text NOT NULL,
    avatar text,
    managed boolean NOT NULL,
	avatar_source text,
	joined timestamp without time zone NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT users_profile_to_auth FOREIGN KEY (id)
        REFERENCES public.users_auth (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);

ALTER TABLE public.users_profile
    ALTER COLUMN managed SET DEFAULT False;

CREATE TABLE public.makers
(
    id bigint NOT NULL,
    type integer NOT NULL,
    name text,
    thumbnail text,
    description text,
    meta jsonb,
    PRIMARY KEY (id)
);

CREATE TABLE public.tags
(
    id bigint NOT NULL,
    name text NOT NULL,
    aliases text[],
    description text,
    icon text,
    parent_id bigint,
    fullname text,
    allowed_maker_types integer[],
    disallowed_maker_types integer[],
    PRIMARY KEY (id)
);

ALTER TABLE public.tags
    ADD CONSTRAINT unique_tags_name_parent UNIQUE (name, parent_id);

ALTER TABLE public.tags
    ADD CONSTRAINT fk_tags_parent_exists FOREIGN KEY (parent_id)
    REFERENCES public.tags (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE
    NOT VALID;

CREATE OR REPLACE VIEW public.tag_names
 AS
SELECT id, parent_id, name, description FROM tags UNION SELECT id, parent_id, unnest(aliases) AS name, description FROM tags;

CREATE TABLE public.tag_assignments
(
    tag_id bigint NOT NULL,
    maker_id bigint NOT NULL,
    rating smallint NOT NULL,
    CONSTRAINT unique_tag_assignment UNIQUE (tag_id, maker_id),
    CONSTRAINT fk_tag_assignment_tag_id FOREIGN KEY (tag_id)
        REFERENCES public.tags (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT fk_tag_assignment_maker_id FOREIGN KEY (maker_id)
        REFERENCES public.makers (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

CREATE TABLE public.audit_log_entries
(
    id bigint NOT NULL,
    type integer NOT NULL,
    details text NOT NULL,
    "time" timestamp without time zone NOT NULL,
    users_id bigint NOT NULL,
    "from" text,
    now text NOT NULL,
    PRIMARY KEY (id)
);