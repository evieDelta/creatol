// Code generated by sqlc. DO NOT EDIT.
// source: user_session.sql

package models

import (
	"context"
	"database/sql"
	"time"
)

const userSessionCreate = `-- name: UserSessionCreate :exec
INSERT INTO users_session (
	session_id, user_id, key, invalid_after, ip_address, user_agent, last_used
) VALUES (
	$1,
	$2,
	$3,
	$4,
	$5,
	$6,
	$7
)
`

type UserSessionCreateParams struct {
	SessionID    int64
	UserID       int64
	Key          []byte
	InvalidAfter time.Time
	IpAddress    string
	UserAgent    string
	LastUsed     time.Time
}

func (q *Queries) UserSessionCreate(ctx context.Context, arg UserSessionCreateParams) error {
	_, err := q.db.Exec(ctx, userSessionCreate,
		arg.SessionID,
		arg.UserID,
		arg.Key,
		arg.InvalidAfter,
		arg.IpAddress,
		arg.UserAgent,
		arg.LastUsed,
	)
	return err
}

const userSessionGetJoinAuth = `-- name: UserSessionGetJoinAuth :one
SELECT session_id, user_id, key, invalid_after, last_used, ip_address, user_agent, id, source, login_method, passkey, meta FROM users_session
JOIN users_auth ON users_session.user_id=users_auth.id
WHERE
	key = $1
`

type UserSessionGetJoinAuthRow struct {
	SessionID    int64
	UserID       int64
	Key          []byte
	InvalidAfter time.Time
	LastUsed     time.Time
	IpAddress    string
	UserAgent    string
	ID           int64
	Source       sql.NullString
	LoginMethod  int16
	Passkey      []byte
	Meta         []byte
}

func (q *Queries) UserSessionGetJoinAuth(ctx context.Context, session []byte) (UserSessionGetJoinAuthRow, error) {
	row := q.db.QueryRow(ctx, userSessionGetJoinAuth, session)
	var i UserSessionGetJoinAuthRow
	err := row.Scan(
		&i.SessionID,
		&i.UserID,
		&i.Key,
		&i.InvalidAfter,
		&i.LastUsed,
		&i.IpAddress,
		&i.UserAgent,
		&i.ID,
		&i.Source,
		&i.LoginMethod,
		&i.Passkey,
		&i.Meta,
	)
	return i, err
}

const userSessionGetSafeByUser = `-- name: UserSessionGetSafeByUser :many
SELECT session_id, user_id, invalid_after, last_used, ip_address, user_agent
FROM users_session
WHERE user_id = $1
`

type UserSessionGetSafeByUserRow struct {
	SessionID    int64
	UserID       int64
	InvalidAfter time.Time
	LastUsed     time.Time
	IpAddress    string
	UserAgent    string
}

func (q *Queries) UserSessionGetSafeByUser(ctx context.Context, id int64) ([]UserSessionGetSafeByUserRow, error) {
	rows, err := q.db.Query(ctx, userSessionGetSafeByUser, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []UserSessionGetSafeByUserRow
	for rows.Next() {
		var i UserSessionGetSafeByUserRow
		if err := rows.Scan(
			&i.SessionID,
			&i.UserID,
			&i.InvalidAfter,
			&i.LastUsed,
			&i.IpAddress,
			&i.UserAgent,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const userSessionInvalidate = `-- name: UserSessionInvalidate :exec
DELETE FROM users_session WHERE
	session_id = $1
`

func (q *Queries) UserSessionInvalidate(ctx context.Context, id int64) error {
	_, err := q.db.Exec(ctx, userSessionInvalidate, id)
	return err
}

const userSessionInvalidateKey = `-- name: UserSessionInvalidateKey :one
DELETE FROM users_session WHERE
	key = $1
RETURNING session_id
`

func (q *Queries) UserSessionInvalidateKey(ctx context.Context, key []byte) (int64, error) {
	row := q.db.QueryRow(ctx, userSessionInvalidateKey, key)
	var session_id int64
	err := row.Scan(&session_id)
	return session_id, err
}

const userSessionPrune = `-- name: UserSessionPrune :exec
DELETE FROM users_session WHERE
	invalid_after < $1
`

func (q *Queries) UserSessionPrune(ctx context.Context, time time.Time) error {
	_, err := q.db.Exec(ctx, userSessionPrune, time)
	return err
}

const userSessionUpdate = `-- name: UserSessionUpdate :exec
UPDATE users_session
	SET invalid_after = $1, last_used = $2
	WHERE session_id = $3
`

type UserSessionUpdateParams struct {
	Expire   time.Time
	LastUsed time.Time
	ID       int64
}

func (q *Queries) UserSessionUpdate(ctx context.Context, arg UserSessionUpdateParams) error {
	_, err := q.db.Exec(ctx, userSessionUpdate, arg.Expire, arg.LastUsed, arg.ID)
	return err
}
