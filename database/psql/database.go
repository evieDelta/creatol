package psql

import (
	"context"
	"database/sql"

	"codeberg.org/eviedelta/creatol"
	"codeberg.org/eviedelta/creatol/database/psql/internal/models"
	"github.com/jackc/pgx/v4/pgxpool"
)

//go:generate go run github.com/kyleconroy/sqlc/cmd/sqlc generate

var _ creatol.Database = &Database{}
var _ creatol.AuthDatabase = &Database{}

type Database struct {
	Conn *pgxpool.Pool
	m    *models.Queries
}

func (d *Database) Open(ctx context.Context, connstr string) error {
	conn, err := pgxpool.Connect(ctx, connstr)
	if err != nil {
		return err
	}
	d.Conn = conn

	d.m = models.New(conn)

	return conn.Ping(ctx)
}

func (d *Database) Close(ctx context.Context) error {
	d.Conn.Close()
	return nil
}

func nullString(s string) sql.NullString {
	return sql.NullString{Valid: s != "", String: s}
}
