-- name: TagAssign :exec
INSERT INTO public.tag_assignments (
	tag_id, maker_id, rating
) VALUES (
	tag_id   = @tag_id,
	maker_id = @maker_id,
	rating   = @rating
);

-- name: TagUnassign :exec
DELETE FROM public.tag_assignments WHERE
	    maker_id = @maker_id
	AND tag_id   = @tag_id
;

-- name: TagAssignUpdate :exec
UPDATE public.tag_assignments
	SET rating   = @new_rating
	WHERE
		maker_id = @maker_id
	AND tag_id   = @tag_id
;