-- name: MakerCreate :exec
INSERT INTO public.makers (
	id, type, name, thumbnail, description, meta
) VALUES (
	 id          = @id
	,type        = @type
	,name        = @name
	,thumbnail   = @thumbnail
	,description = @description
	,meta        = @meta
);

-- name: MakerGetByID :one
SELECT * FROM public.makers WHERE
	id = @id
;

-- name: MakerListByID :many
SELECT * FROM public.makers
	WHERE id > @after
	ORDER BY id ASC
	LIMIT    @max
;
