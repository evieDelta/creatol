-- name: UserSessionGetJoinAuth :one
SELECT * FROM users_session
JOIN users_auth ON users_session.user_id=users_auth.id
WHERE
	key = @session
;

-- name: UserSessionGetSafeByUser :many
SELECT session_id, user_id, invalid_after, last_used, ip_address, user_agent
FROM users_session
WHERE user_id = @id
;

-- name: UserSessionCreate :exec
INSERT INTO users_session (
	session_id, user_id, key, invalid_after, ip_address, user_agent, last_used
) VALUES (
	@session_id,
	@user_id,
	@key,
	@invalid_after,
	@ip_address,
	@user_agent,
	@last_used
);

-- name: UserSessionInvalidate :exec
DELETE FROM users_session WHERE
	session_id = @id
;

-- name: UserSessionInvalidateKey :one
DELETE FROM users_session WHERE
	key = @key
RETURNING session_id
;

-- name: UserSessionPrune :exec
DELETE FROM users_session WHERE
	invalid_after < @time
;

-- name: UserSessionUpdate :exec
UPDATE users_session
	SET invalid_after = @expire, last_used = @last_used
	WHERE session_id = @id
;

;