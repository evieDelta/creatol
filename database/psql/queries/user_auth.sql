-- name: UserAuthGetBySource :one
SELECT * FROM public.users_auth WHERE source = $1 AND login_method = $2;

-- name: UserAuthGetByID :one
SELECT * FROM public.users_auth WHERE id = $1;

-- name: UserAuthCreate :exec
INSERT INTO public.users_auth (
	id, source, login_method, passkey, meta
) VALUES (
	$1, $2, $3, $4, $5
);

-- name: UserAuthUpdate :exec
UPDATE public.users_auth SET
	source       = @new_source,
	passkey      = @new_key,
	login_method = @new_method,
	meta         = @new_meta
	WHERE id = @id AND passkey = @old_key AND source = @old_source;

-- name: UserAuthDelete :exec
UPDATE public.users_auth SET
	source       = null,
	passkey      = null,
	login_method = 0
	WHERE id = @id;

;