-- name: UserProfileGetFullByID :one
SELECT * FROM public.users_profile
	WHERE id = $1
;

-- name: UserProfileGetByID :one
SELECT id, name, avatar, joined FROM public.users_profile
	WHERE id = @id
;

-- name: UserProfilesGetPaginate :many
SELECT id, name, avatar, joined FROM public.users_profile
	WHERE id > @after
	LIMIT @max
;

---- name: UserProfileGetByName :many
--SELECT * FROM public.users_profile
--	WHERE name ILIKE @query::text
--	ORDER BY id
--	LIMIT @limit ASC
--	PAGE @page;

-- name: UserProfileCreate :exec
INSERT INTO public.users_profile (
	id, name, avatar, managed, avatar_source, joined
) VALUES (
	$1, $2, $3, $4, $5, $6
);

-- name: UserProfileUpdate :exec
UPDATE public.users_profile SET
	avatar        = @avatar,
	name          = @name,
	avatar_source = @avatar_source,
	managed       = @managed
	WHERE
		id      = @id
;

-- name: UserProfileDisable :exec
UPDATE public.users_profile SET
	avatar        = null,
	avatar_source = null,
	name          = @name
	WHERE
		id      = @id
;
