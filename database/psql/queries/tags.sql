;-- name: TagsCreate :exec
INSERT INTO public.tags (
	 id, name, aliases, description, icon, parent_id
	,allowed_maker_types, disallowed_maker_types
) VALUES (
	 id          = @id
	,name        = @name
	,aliases     = @aliases
	,description = @description
	,icon        = @icon
	,parent_id   = @parent_id
	
	,allowed_maker_types    = @allowed_maker_types
	,disallowed_maker_types = @disallowed_maker_types
)

;-- name: TagsGetByID :one
SELECT * FROM public.tags WHERE
	id = $1;

;-- name: TagsListByID :many
SELECT * FROM public.tags WHERE
	    id > @after
	
	ORDER BY id ASC
	LIMIT    @max

;-- name: TagsListByParentAndID :many
SELECT * FROM public.tags WHERE
	    id        > @after
	AND parent_id = @parent
	
	ORDER BY id ASC
	LIMIT    @max

;-- name: TagsListByText :many
SELECT * FROM public.tags WHERE
		"name"     ILIKE @text
--  OR  "fullname" ILIKE @prefix
	AND "name"     >     @after
	
	ORDER BY name ASC
	LIMIT    @max

;