package psql

import (
	"context"
	"time"

	"codeberg.org/eviedelta/creatol"
	"codeberg.org/eviedelta/creatol/constants"
	"codeberg.org/eviedelta/creatol/database/psql/internal/models"
	"emperror.dev/errors"
)

func (d *Database) RegisterUser(ctx context.Context, auth creatol.UserAuth, prof creatol.UserProfileMeta) error {
	l := creatol.GetQueryStat(ctx).Meta("begin")
	tx, err := d.Conn.Begin(ctx)
	l()
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)
	t := d.m.WithTx(tx)

	l = creatol.GetQueryStat(ctx).Query("auth create")
	err = t.UserAuthCreate(ctx, models.UserAuthCreateParams{
		ID:          int64(auth.ID),
		Source:      nullString(auth.SourceID),
		LoginMethod: int16(auth.AuthType),
		Passkey:     auth.Passkey,
		Meta:        auth.Meta,
	})
	l()
	if err != nil {
		return err
	}

	l = creatol.GetQueryStat(ctx).Query("profile create")
	err = t.UserProfileCreate(ctx, models.UserProfileCreateParams{
		ID:           int64(prof.ID),
		Name:         prof.Name,
		Avatar:       nullString(prof.Avatar),
		Managed:      prof.Managed,
		AvatarSource: nullString(prof.AvatarSource),
		Joined:       prof.Joined,
	})
	l()
	if err != nil {
		return err
	}

	l = creatol.GetQueryStat(ctx).Meta("commit")
	defer l()

	return tx.Commit(ctx)
}

func (d *Database) GetUserAuthBySource(ctx context.Context, source string, loty constants.AuthMethod) (creatol.UserAuth, error) {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	a, err := d.m.UserAuthGetBySource(ctx, models.UserAuthGetBySourceParams{
		Source:      nullString(source),
		LoginMethod: int16(loty),
	})
	if err != nil {
		return creatol.UserAuth{}, err
	}

	pl := creatol.UserAuth{
		ID:       creatol.UserID(a.ID),
		SourceID: a.Source.String,
		AuthType: constants.AuthMethod(a.LoginMethod),
		Passkey:  a.Passkey,
		Meta:     a.Meta,
	}

	return pl, nil
}

func (d *Database) UpdateUserAuth(ctx context.Context, old, new creatol.UserAuth) error {
	if old.ID != new.ID {
		return errors.NewPlain("mismatched IDs, ID change is not a permitted action")
	}

	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	tx, err := d.Conn.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)
	t := d.m.WithTx(tx)

	err = t.UserAuthUpdate(ctx, models.UserAuthUpdateParams{
		NewSource: nullString(new.SourceID),
		NewKey:    new.Passkey,
		NewMethod: int16(new.AuthType),
		NewMeta:   new.Meta,
		ID:        int64(old.ID),
		OldKey:    old.Passkey,
		OldSource: nullString(old.SourceID),
	})

	if err != nil {
		return err
	}

	return tx.Commit(ctx)
}

// DeleteAccount permamently and irreversably locks an account
func (d *Database) DeleteAccount(ctx context.Context, uid creatol.UserID, delname string) error {
	l := creatol.GetQueryStat(ctx).Meta("begin")
	tx, err := d.Conn.Begin(ctx)
	l()
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)
	t := d.m.WithTx(tx)

	l = creatol.GetQueryStat(ctx).Query("delete profile")
	err = t.UserProfileDisable(ctx, models.UserProfileDisableParams{
		ID:   int64(uid),
		Name: delname,
	})
	l()
	if err != nil {
		return err
	}

	l = creatol.GetQueryStat(ctx).Query("delete auth")
	err = t.UserAuthDelete(ctx, int64(uid))
	l()
	if err != nil {
		return err
	}

	l = creatol.GetQueryStat(ctx).Meta("commit")
	defer l()
	return tx.Commit(ctx)
}

func (d *Database) NewSession(ctx context.Context, us creatol.UserSession) error {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	tx, err := d.Conn.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)
	t := d.m.WithTx(tx)

	err = t.UserSessionCreate(ctx, models.UserSessionCreateParams{
		SessionID:    int64(us.ID),
		UserID:       int64(us.UserID),
		Key:          us.Key,
		InvalidAfter: us.InvalidAfter,
		IpAddress:    us.IPAddress,
		UserAgent:    us.UserAgent,
		LastUsed:     us.LastUsed,
	})
	if err != nil {
		return err
	}

	return tx.Commit(ctx)
}

func (d *Database) GetSession(ctx context.Context, ses []byte) (s creatol.UserSession, a creatol.UserAuth, err error) {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	o, err := d.m.UserSessionGetJoinAuth(ctx, ses)
	if err != nil {
		return s, a, err
	}

	return creatol.UserSession{
			UserID:       creatol.UserID(o.ID),
			Key:          o.Key,
			InvalidAfter: o.InvalidAfter,
			IPAddress:    o.IpAddress,
			UserAgent:    o.UserAgent,
			LastUsed:     o.LastUsed,
		}, creatol.UserAuth{
			ID:       creatol.UserID(o.ID),
			SourceID: o.Source.String,
			AuthType: constants.AuthMethod(o.LoginMethod),
			Passkey:  o.Passkey,
			Meta:     o.Meta,
		}, nil
}

// GetSessionsByUser gets session data from a user, except for the key
func (d *Database) GetSessionsByUser(ctx context.Context, uid creatol.UserID) (s []creatol.UserSession, err error) {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	o, err := d.m.UserSessionGetSafeByUser(ctx, int64(uid))
	if err != nil {
		return nil, err
	}

	for _, x := range o {
		s = append(s, creatol.UserSession{
			UserID:       creatol.UserID(x.UserID),
			InvalidAfter: x.InvalidAfter,
			IPAddress:    x.IpAddress,
			UserAgent:    x.UserAgent,
			LastUsed:     x.LastUsed,
		})
	}

	return s, nil
}

func (d *Database) UpdateSession(ctx context.Context, id creatol.SessionID, expire, use time.Time) error {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	return d.m.UserSessionUpdate(ctx, models.UserSessionUpdateParams{
		ID:       int64(id),
		Expire:   expire,
		LastUsed: use,
	})
}

func (d *Database) PruneSessions(ctx context.Context) error {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	tx, err := d.Conn.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)
	t := d.m.WithTx(tx)

	err = t.UserSessionPrune(ctx, time.Now())
	if err != nil {
		return err
	}

	return tx.Commit(ctx)
}

func (d *Database) InvalidateSession(ctx context.Context, sid creatol.SessionID) error {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	return d.m.UserSessionInvalidate(ctx, int64(sid))
}

func (d *Database) InvalidateSessionKey(ctx context.Context, key []byte) (creatol.SessionID, error) {
	l := creatol.GetQueryStat(ctx).Query("")
	defer l()

	sid, err := d.m.UserSessionInvalidateKey(ctx, key)
	return creatol.SessionID(sid), err
}
