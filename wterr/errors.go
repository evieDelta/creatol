package wterr

import (
	"fmt"
	"net/http"
	"strings"

	"emperror.dev/errors"
)

// we use this little known ascii control character as a separator instead of any visible one
// to reduce the chances of wterr's functions interfering with the error contents
const unitSeparator string = "\x1F"

type UserError interface {
	error

	ErrStr() string // ErrStr returns just the error part (containing the actual error details)
	MsgStr() string // MsgStr returns just the msg part (contains a string that may be shown to a user)
	StatusCodeStr() string
	StatusCode() int
}

// IsUserError checks if it is a wterr type error
// and if so returns the interface that allows you to access the individual fields
// (particularly the fields for determining how it is shown to an end user)
func IsUserError(err error) (UserError, bool) {
	ue, ok := err.(UserError)
	return ue, ok
}

// Error is an error type
type Error struct {
	Type int
	Err  error
	Str  string
}

func (e Error) Error() string {
	return http.StatusText(e.Type) + ": " + e.MsgStr() + ": " + e.ErrStr()
}

// ErrStr returns just the error part (containing the actual error details)
func (e Error) ErrStr() string {
	if e.Err == nil {
		return ""
	}
	return strings.ReplaceAll(e.Err.Error(), unitSeparator, " & ")
}

// MsgStr returns just the msg part (contains a string that may be shown to a user)
func (e Error) MsgStr() string { return strings.ReplaceAll(e.Str, unitSeparator, " & ") }

// StatusCodeStr returns the status code formatted as a string via http.StatusText
func (e Error) StatusCodeStr() string { return http.StatusText(e.Type) }
func (e Error) StatusCode() int       { return e.Type }

type ErrorBuilder struct {
	e Error
}

func New(status int) *ErrorBuilder {
	if status == 0 {
		status = http.StatusInternalServerError
	}
	return &ErrorBuilder{
		e: Error{Type: status},
	}
}

func (b *ErrorBuilder) Error() string {
	return b.Get().Error()
}

// Get finalises the error and returns it using the UserError interface
func (b *ErrorBuilder) Get() UserError { return b.e }

func (b *ErrorBuilder) ErrorRaw(err error) *ErrorBuilder {
	if e, ok := err.(Error); ok {
		if b.e.Err == nil {
			b.e = e
			return b
		}
		b.ErrorRaw(e.Err)
		b.MsgRaw(e.Str)
		return b
	}

	if b.e.Err != nil {
		b.e.Err = errors.Wrap(b.e.Err, err.Error())
	} else {
		b.e.Err = err
	}

	return b
}

func (b *ErrorBuilder) Err(i ...interface{}) *ErrorBuilder {
	t := fmt.Sprint(i...)

	return b.ErrorRaw(errors.New(t))
}

func (b *ErrorBuilder) Errorln(i ...interface{}) *ErrorBuilder {
	t := fmt.Sprintln(i...)

	return b.ErrorRaw(errors.New(t[:len(t)-1]))
}

func (b *ErrorBuilder) Errorf(f string, i ...interface{}) *ErrorBuilder {
	t := fmt.Sprintf(f, i...)

	return b.ErrorRaw(errors.New(t))
}

func (b *ErrorBuilder) MsgRaw(msg string) *ErrorBuilder {
	if b.e.Str != "" {
		b.e.Str += string(unitSeparator) + msg
	} else {
		b.e.Str = msg
	}

	return b
}

func (b *ErrorBuilder) Msg(i ...interface{}) *ErrorBuilder {
	t := fmt.Sprint(i...)

	return b.MsgRaw(t)
}

func (b *ErrorBuilder) Msgln(i ...interface{}) *ErrorBuilder {
	t := fmt.Sprintln(i...)

	return b.MsgRaw(t[:len(t)-1])
}

func (b *ErrorBuilder) Msgf(f string, i ...interface{}) *ErrorBuilder {
	t := fmt.Sprintf(f, i...)

	return b.MsgRaw(t)
}
